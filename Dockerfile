FROM node:9-alpine

RUN apk add --no-cache curl

WORKDIR /opt/api
COPY . /opt/api

ARG NPM_TOKEN
ENV NPM_TOKEN $NPM_TOKEN
ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
RUN echo "//registry.npmjs.org/:_authToken=\${NPM_TOKEN}" > /root/.npmrc

COPY package.json /opt/api
RUN apk add --no-cache make gcc g++ python && \
  npm install && \
  npm cache clean --force && \
  apk del make gcc g++ python
RUN rm -f /root/.npmrc
COPY . /opt/api

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/opt/api/bin/dumb-init", "--"]
CMD ["npm", "start"]
