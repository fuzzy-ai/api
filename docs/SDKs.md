## Fuzzy.ai SDK Design

This document is intended to provide a general guideline for the development of SDKs. The "code" below is language independent.

### class Client

#### Properties
  * `apiKey`
  * `apiRoot`

#### Methods

  * `constructor(key, root=null)`
  * `evaluate(id, inputs)` - returns `Evaluation` object
  * `feedback(evalID, feedback)` - returns `Feedback` object
  * `getAgent(id)` - returns `Agent` object from API
  * `getVersion()` - returns current API version

### class Agent

#### Properties
  * `id`
  * `inputs`
  * `outputs`
  * ?? `rules` (Parsed or regular?)

#### Methods
  * `evaluate(inputs)` - returns `Evaluation` object
  * `feedback(id, feedback)` - returns `Feedback` object
  * `create(props)`
  * `read(id)`
  * `update(props)`
  * `delete()`

### class Evaluation

#### Properties
  * `id`
  * `inputs`
  * `outputs`

#### Methods
  * `read(id)` - returns `Evaluation` object

### class Feedback

#### Properties
  * `id`
  * `metrics`

#### Methods
  * `read(id)` - returns `Feedback` object

### Other considerations

  * Sync vs Async - use the latter if it's language native (e.g. nodejs).
  * read only / evaluate only API keys - see
