## Configuration options

The API server is a subclass of
[Microservice](https://gitlab.com/fuzzy-ai/microservice), so it uses all the
same environment variables for basic configuration.

It also uses these:

-   URL_PREFIX: When generating URLs, like for Location: headers, this is
    the URL prefix.

-   ENSURE_USER: A user account to make sure exists. Should be only for testing.
    No defaults. Format is "<id>:<email>".

-   ENSURE_ACCESS_TOKEN: An access token to make sure exists. Only for testing.
    No defaults. Will only work if ENSURE_USER is set; will be assigned to that
    user.

-   ILLEGAL_AGENT_ID: Agent ID to ensure doesn't exist. Fires off an error. For
    testing errors.

-   WEBCLIENT_TIMEOUT: Timeout for keepalive connections in the web client.
    Default is `Infinity`; that is, keep the connections alive as long as the
    server will let us.

-   STATS_SERVER: Root URL of the stats server.

-   STATS_KEY: API key to use for the stats server.

-   STATS_TIMEOUT: Time to wait for the stats server before failing. Default is
    5 seconds.

-   PLAN_SERVER: Root URL for the plan server.

-   PLAN_KEY: API key to use for the plan server.

-   EVALUATION_TIMEOUT: max time to wait for an evaluation before timing out.
    Defaults to 60 seconds, which is really high.

-   EVALUATOR_SERVER: server to use for evaluations.

-   EVALUATOR_KEY: API key to use for evaluator server.

-   EVALUATOR_CONCURRENCY: How many concurrent requests to send to evaluators at
    a time. Default is 16. Additional requests will be queued. A good rule of
    thumb might be to use the number of evaluators available divided by 2.

-   DOCS_URL: URL for the documentation for this server. Used for the root
    document.

-   REDIRECT_TO_HTTPS: when this is truthy, http requests will be redirected
    to https with the same host and path.

-   LEARNING_KEY: API key to use for learning microservice.

-   LEARNING_SERVER: root URL for the learning microservice.

## Parsed rules

Agents are returned with an array of parsed_rules; they are objects with the
following format.

Each object in the hierarchy is marked with a "type" property that gives its
type, as a string. Existing types:

-   "if-then": an IF-THEN rule. It has three properties:
    -   antecedent: an expression of multiple possible types; the IF part.

    -   consequent: an IS expression; the THEN part.

    -   weight: the weight of the rule; always a floating-point number.
        Optional; if left out, weight is 1.0.

-   "increases": an INCREASES rule. It has three properties:

    -   input: the input; a string.

    -   output: the output; a string.

    -   weight: the weight of the rule; either a floating-point number or an
        array of floating point numbers. If an array, represents weights of
        component rules from lowest to highest. Optional; if left out, weight is
        1.0.

-   "decreases": a DECREASES rule

    -   input: the input; a string.

    -   output: the output; a string.

    -   weight: the weight of the rule; either a floating-point number or an
        array of floating point numbers. If an array, represents weights of
        component rules from lowest to highest. Optional; if left out, weight is
        1.0.

-   "is": an IS expression, like "temperature IS hot". Two properties:

    -   dimension: the dimension measured as a string. Here, "temperature".
    -   set: The set that the dimension is in as a string. Here, "hot".

-   "not": a NOT expression, like "NOT temperature IS hot". One property:

    -   expression: the expression negated, as an object. So, here,
        {type: "is", dimension: "temperature", set: "hot"}.

-   "and": an AND expression, like "temperature IS hot AND pressure IS high".

    -   left: the expression on the left, as an object.

    -   right: the expression on the right, as an object.

-   "or": an OR expression, like "temperature IS hot OR pressure IS high"

    -   left: the expression on the left, as an object.

    -   right: the expression on the right, as an object.

Note that the expression tree is recursive and can go to arbitrary depth; don't
expect "left" and "right" always to be "is" expressions, for example.
