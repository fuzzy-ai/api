// user.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const db = require('databank')
const async = require('async')
const bcrypt = require('bcryptjs')

const uniqueID = require('./uniqueid')
const timestamp = require('./timestamp')
const AccessToken = require('./accesstoken')
const Agent = require('./agent')
const DeletedUser = require('./deleteduser')
const DeletedAgent = require('./deletedagent')

class EmailExistsError extends Error {
  constructor (email) {
    super(`Already have a user for ${email}`)
    this.email = email
    this.name = 'EmailExistsError'
    this.statusCode = 409
  }
}

const User = db.DatabankObject.subClass('user')

User.schema = {
  pkey: 'id',
  fields: [
    'email',
    'passwordHash',
    'createdAt',
    'updatedAt'
  ],
  indices: ['email']
}

User.beforeCreate = function (props, callback) {
  if (!props.email) {
    callback(new Error('email property required'))
    return
  }

  props.createdAt = (props.updatedAt = timestamp())

  return async.waterfall([
    callback => uniqueID(callback),
    function (id, callback) {
      if ((props.id == null)) {
        props.id = id
      }
      return callback(null)
    },
    callback =>
      User.byEmail(props.email, (err, user) => {
        if (!err || (err.name !== 'NoSuchThingError')) {
          return callback(new EmailExistsError(props.email))
        } else {
          return callback(null)
        }
      }),
    function (callback) {
      if (!props.password) {
        return callback(null)
      } else {
        return async.waterfall([
          callback => bcrypt.genSalt(10, callback),
          (salt, callback) => bcrypt.hash(props.password, salt, callback)
        ], (err, passwordHash) => {
          if (err) {
            return callback(err)
          } else {
            props.passwordHash = passwordHash
            delete props.password
            return callback(null)
          }
        })
      }
    }
  ], (err) => {
    if (err) {
      return callback(err)
    } else {
      return callback(null, props)
    }
  })
}

User.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = timestamp()

  return callback(null, props)
}

User.prototype.getAgents = function (callback) {
  return Agent.search({userID: this.id}, callback)
}

User.prototype.getDeletedAgents = function (callback) {
  return DeletedAgent.search({userID: this.id}, callback)
}

User.prototype.afterDel = function (callback) {
  return async.waterfall([
    callback => {
      return DeletedUser.fromUser(this, callback)
    },
    (deleted, callback) => {
      return this.getAgents(callback)
    },
    (agents, callback) => {
      const deleteAgent = (agent, callback) => agent.del(callback)
      return async.eachLimit(agents, 16, deleteAgent, callback)
    },
    callback => {
      return AccessToken.search({userID: this.id}, callback)
    },
    (tokens, callback) => {
      const deleteToken = (token, callback) => token.del(callback)
      return async.eachLimit(tokens, 16, deleteToken, callback)
    }
  ], err => callback(err))
}

User.register = function (props, app, callback) {
  let user = null
  return async.waterfall([
    callback => User.create(props, callback),
    function (results, callback) {
      user = results
      // Default token
      return AccessToken.create({userID: user.id}, callback)
    },
    function (results, callback) {
      return AccessToken.create({userID: user.id, app: app.id}, callback)
    }
  ], (err, appToken) => {
    if (err) {
      return callback(err)
    } else {
      return callback(null, user, appToken.token)
    }
  })
}

User.byEmail = (email, callback) =>
  User.search({email}, (err, users) => {
    if (err) {
      callback(err)
    } else if (!users || (users.length === 0)) {
      return callback(new db.NoSuchThingError(`Could not find user for ${email}`))
    } else if (users.length > 1) {
      return callback(new Error(`Too many users for ${email}`))
    } else {
      return callback(null, users[0])
    }
  })

User.login = function (email, password, app, callback) {
  let user = null
  return async.waterfall([
    callback => User.byEmail(email, callback),
    function (results, callback) {
      user = results
      return bcrypt.compare(password, user.passwordHash, callback)
    },
    function (passwordCorrect, callback) {
      if (!passwordCorrect) {
        return callback(new Error('Incorrect password'))
      } else {
        return AccessToken.create({userID: user.id, app: app.id}, callback)
      }
    }
  ], (err, appToken) => {
    if (err) {
      return callback(err)
    } else {
      return callback(null, user, appToken.token)
    }
  })
}

module.exports = User
