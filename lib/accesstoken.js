// accesstoken.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const crypto = require('crypto')

const db = require('databank')

const timestamp = require('./timestamp')

const AccessToken = db.DatabankObject.subClass('accesstoken')

AccessToken.schema = {
  pkey: 'token',
  fields: [
    'userID',
    'app',
    'createdAt',
    'updatedAt'
  ],
  indices: ['userID', 'app']
}

AccessToken.beforeCreate = function (props, callback) {
  props.createdAt = (props.updatedAt = timestamp())

  if (props.token) {
    return callback(null, props)
  } else {
    return crypto.randomBytes(8, (err, buf) => {
      if (err) {
        return callback(err, null)
      } else {
        const n1 = buf.readUInt32BE(0)
        const n2 = buf.readUInt32BE(4)
        props.token = n1.toString(36) + n2.toString(36)
        return callback(null, props)
      }
    })
  }
}

AccessToken.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = timestamp()

  return callback(null, props)
}

module.exports = AccessToken
