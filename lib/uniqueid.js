// uniqueid.js
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const crypto = require('crypto')
const base32 = require('hi-base32')

const idify = buf => base32.encode(buf).replace(/=+$/, '')

const uniqueID = function (callback) {
  if (callback != null) {
    return crypto.randomBytes(16, (err, buf) => {
      if (err) {
        return callback(err)
      } else {
        return callback(null, idify(buf))
      }
    })
  } else {
    return idify(crypto.randomBytes(16))
  }
}

module.exports = uniqueID
