// deletedagent.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const db = require('databank')

const timestamp = require('./timestamp')

const DeletedAgent = db.DatabankObject.subClass('DeletedAgent')

DeletedAgent.schema = {
  pkey: 'id',
  fields: [
    'userID',
    'createdAt',
    'updatedAt',
    'deletedAt'
  ],
  indices: ['userID']
}

DeletedAgent.beforeCreate = function (props, callback) {
  try {
    assert(_.isString(props.id), "No 'id' in new DeletedAgent")
    assert(_.isString(props.userID), "No 'userID' in new DeletedAgent")
    assert(_.isString(props.createdAt), "No 'createdAt' in new DeletedAgent")
    assert(_.isString(props.updatedAt), "No 'updatedAt' in new DeletedAgent")
  } catch (err) {
    callback(err, null)
    return
  }

  props.deletedAt = timestamp()

  return callback(null, props)
}

DeletedAgent.fromController = function (controller, callback) {
  const props = _.pick(controller, 'id', 'userID', 'createdAt', 'updatedAt')

  return DeletedAgent.create(props, callback)
}

module.exports = DeletedAgent
