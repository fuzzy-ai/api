// learning-client.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const MicroserviceClient = require('@fuzzy-ai/microservice-client')

class LearningClient extends MicroserviceClient {
  version (callback) {
    return this.get('/version', callback)
  }

  optimize (properties, inputs, expected, callback) {
    const payload = {
      agent: properties,
      inputs,
      expected
    }

    return this.post('/optimize', payload, callback)
  }
}

module.exports = LearningClient
