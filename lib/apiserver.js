// apiserver.js
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')
const url = require('url')

const _ = require('lodash')
const async = require('async')
const cors = require('cors')
const Microservice = require('@fuzzy-ai/microservice')
const uuid = require('uuid')
const debug = require('debug')('api:apiserver')
const {WebClient} = require('@fuzzy-ai/web')

const AccessToken = require('./accesstoken')
const Agent = require('./agent')
const AgentVersion = require('./agentversion')
const DeletedAgent = require('./deletedagent')
const DeletedAgentVersion = require('./deletedagentversion')
const DeletedUser = require('./deleteduser')
const HTTPError = require('./httperror')
const PlanClient = require('./plan-client')
const RFC = require('@fuzzy-ai/fuzzy-controller')
const StatsClient = require('./statsclient')
const EvaluatorClient = require('./evaluator-client')
const User = require('./user')
const version = require('./version')
const LearningClient = require('./learning-client')

const YEAR = 365 * 24 * 60 * 60 * 1000

const MAX_BATCH_SIZE = 4096

const js = JSON.stringify

class APIServer extends Microservice {
  getName () {
    return 'api'
  }

  environmentToConfig (env) {
    const config = super.environmentToConfig(env)

    _.extend(config, {
      urlPrefix: env['URL_PREFIX'],
      ensureUser: env['ENSURE_USER'],
      ensureAccessToken: env['ENSURE_ACCESS_TOKEN'],
      illegalAgentID: env['ILLEGAL_AGENT_ID'],
      webClientTimeout: this.envInt(env, 'WEBCLIENT_TIMEOUT', Infinity),
      statsServer: env['STATS_SERVER'] || 'http://stats/',
      statsKey: env['STATS_KEY'],
      statsTimeout: this.envInt(env, 'STATS_TIMEOUT', 5),
      evaluationTimeout: this.envInt(env, 'EVALUATION_TIMEOUT', 30),
      planServer: env['PLAN_SERVER'] || 'http://plan/',
      planKey: env['PLAN_KEY'],
      evaluatorKey: env['EVALUATOR_KEY'] || 'http://evaluator/',
      evaluatorServer: env['EVALUATOR_SERVER'],
      evaluatorConcurrency: this.envInt(env, 'EVALUATOR_CONCURRENCY', 16),
      docsURL: env['DOCS_URL'] || 'https://fuzzy.ai/docs',
      redirectToHttps: this.envBool(env, 'REDIRECT_TO_HTTPS', false),
      learningKey: env['LEARNING_KEY'],
      learningServer: env['LEARNING_SERVER'] || 'http://learning/'
    })

    return config
  }

  setupExpress () {
    const exp = super.setupExpress()
    exp.webClient = new WebClient({timeout: this.config.webClientTimeout})

    // Log errors from microservice clients
    // These can be transitory so we just show them as warnings

    const clientLog = (client, name) => {
      return client.on('error', err => {
        const warning = `${err.name}: ${err.message} (from ${name} client)`
        this.slackMessage('warning', warning, ':warning:', (err) => {
          if (err) {
            return exp.log.error({err}, 'Error posting to Slack')
          }
        })
        return exp.log.warn({err}, `Error with ${name} client`)
      })
    }

    // Add a StatsClient

    exp.statsClient = new StatsClient({
      root: this.config.statsServer,
      key: this.config.statsKey,
      webClient: exp.webClient,
      maxWait: this.config.statsTimeout
    })

    clientLog(exp.statsClient, 'stats')

    // Add a PlanClient

    exp.planClient = new PlanClient({
      root: this.config.planServer,
      key: this.config.planKey,
      webClient: exp.webClient
    })

    clientLog(exp.planClient, 'plan')

    // Add an EvaluatorClient

    exp.evaluatorClient = new EvaluatorClient({
      root: this.config.evaluatorServer,
      key: this.config.evaluatorKey,
      webClient: exp.webClient,
      maxWait: this.config.evaluationTimeout,
      queueLength: this.config.evaluatorConcurrency
    })

    clientLog(exp.evaluatorClient, 'evaluator')

    exp.learningClient = new LearningClient({
      root: this.config.learningServer,
      key: this.config.learningKey,
      webClient: exp.webClient
    })

    clientLog(exp.learningClient, 'learning')

    return exp
  }

  setupMiddleware (exp) {
    // Redirect if we're HTTPS and getting hit by HTTP

    if (this.config.redirectToHttps) {
      exp.use((req, res, next) => {
        if (req.headers['x-forwarded-proto'] === 'http') {
          return res.redirect(301, `https://${req.hostname}${req.url}`)
        } else {
          return next()
        }
      })
    }

    exp.use((req, res, next) => {
      debug(`${req.method} ${req.url}`)
      return next()
    })

    const headers = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'

    const corsOptions = {
      allowedHeaders: headers,
      preflightContinue: true
    }

    // Enable CORS on all endpoints

    exp.use(cors(corsOptions))

    return exp
  }

  setupParams (exp) {
    exp.param('userID', this.userID)
    exp.param('agentID', this.agentID)
    exp.param('versionID', this.versionID)
    exp.param('evaluationID', this.evaluationID)

    return exp
  }

  setupRoutes (exp) {
    exp.get('/', this.getRoot.bind(this))

    const upipe = [this.authc.bind(this), this.userIsPrincipal.bind(this)]
    const ppipe = [this.authc.bind(this), this.principalAsUser.bind(this)]

    exp.post('/user/:userID/agent', upipe, this.requireBody.bind(this), this.createAgent.bind(this))
    exp.post('/agent', ppipe, this.requireBody.bind(this), this.createAgent.bind(this))

    exp.get('/user/:userID/agent', this.authc.bind(this), this.userIsPrincipal.bind(this), this.listAgents.bind(this))
    exp.get('/agent', this.authc.bind(this), this.principalAsUser.bind(this), this.listAgents.bind(this))

    // Original version of these URLs was /user/:userID/agent/:agentID
    // but that meant you had to know the user ID to call an agent.
    // Instead, moved everything to /agent/:agentID. Should be better.

    const ua = [upipe, this.userOwnsAgent.bind(this)]
    const pa = [ppipe, this.principalOwnsAgent.bind(this), this.ownerAsUser.bind(this)]

    exp.get('/agent/:agentID', pa, this.getAgent.bind(this))
    exp.get('/user/:userID/agent/:agentID', ua, this.getAgent.bind(this))

    exp.put('/agent/:agentID', pa, this.requireBody.bind(this), this.putAgent.bind(this))
    exp.put('/user/:userID/agent/:agentID', ua, this.requireBody.bind(this), this.putAgent.bind(this))

    const al = [this.ignoreAPILimit.bind(this)]

    exp.post('/agent/:agentID', pa, al, this.requireBody.bind(this), this.postAgent.bind(this))
    exp.post('/user/:userID/agent/:agentID', ua, al, this.requireBody.bind(this), this.postAgent.bind(this))

    exp.delete('/agent/:agentID', pa, this.deleteAgent.bind(this))
    exp.delete('/user/:userID/agent/:agentID', ua, this.deleteAgent.bind(this))

    exp.get('/agent/:agentID/version', pa, this.listAgentVersions.bind(this))
    exp.get('/user/:userID/agent/:agentID/version', ua, this.listAgentVersions.bind(this))

    const uv = [ua, this.versionOfAgent.bind(this)]
    const pv = [ppipe, this.principalOwnsVersion.bind(this)]

    exp.get('/version/:versionID', pv, this.getVersion.bind(this))
    exp.get('/user/:userID/agent/:agentID/version/:versionID', uv, this.getVersion.bind(this))

    const pel = [
      this.authc.bind(this),
      this.reqEvaluationLog.bind(this),
      this.principalOwnsEvaluationLog.bind(this)
    ]

    exp.get('/evaluation/:evaluationID', pel, this.getEvaluationLog.bind(this))

    exp.get('/evaluation/:evaluationID/feedback', pel, this.getFeedback.bind(this))

    const pelb = [pel, this.evaluationLogAgent.bind(this), this.requireBody.bind(this)]

    exp.post('/evaluation/:evaluationID/feedback', pelb, this.postFeedback.bind(this))

    exp.get('/version', this.getAppVersion.bind(this))

    exp.get('/live', this.dontLog.bind(this), this.getLive.bind(this))

    exp.get('/ready', this.dontLog.bind(this), this.getReady.bind(this))

    exp.use((req, res, next) => {
      // OPTIONS passes through to Express default handler.
      if (req.method === 'OPTIONS') {
        return next()
      } else {
        const msg = 'No such route. See https://fuzzy.ai/docs/rest for documentation.'
        return res.status(404).send(msg)
      }
    })

    return exp
  }

  getSchema () {
    // NOTE: for historical reasons, "user" and "accesstoken" types are
    // lowercase.

    const schema = {
      'accesstoken': AccessToken.schema,
      'Agent': Agent.schema,
      'AgentVersion': AgentVersion.schema,
      'DeletedAgent': DeletedAgent.schema,
      'DeletedAgentVersion': DeletedAgentVersion.schema,
      'DeletedUser': DeletedUser.schema,
      'user': User.schema
    }

    return schema
  }

  startCustom (callback) {
    return async.waterfall([
      callback => {
        if (this.config.illegalAgentID) {
          Agent.beforeGet = (id, callback) => {
            if (id === this.config.illegalAgentID) {
              return callback(new Error(`Forbidden ID ${this.config.illegalAgentID}`))
            } else {
              return callback(null, id)
            }
          }
        }
        if (this.config.ensureUser) {
          const [id, email] = Array.from(this.config.ensureUser.split(':'))
          return User.search({id, email}, (err, users) => {
            if (err) {
              return callback(err)
            } else if (users.length > 0) {
              return callback(null, users[0])
            } else {
              return User.create({id, email}, callback)
            }
          })
        } else {
          return callback(null, null)
        }
      },
      (user, callback) => {
        const tok = this.config.ensureAccessToken
        if (user && tok) {
          return AccessToken.search({token: tok}, (err, tokens) => {
            if (err) {
              return callback(err)
            } else if (tokens.length > 0) {
              return callback(null, tokens[0])
            } else {
              const props = {
                token: tok,
                userID: user.id,
                app: 'ensured'
              }
              return AccessToken.create(props, callback)
            }
          })
        } else {
          return callback(null, null)
        }
      }
    ], (err, token) => callback(err))
  }

  stopCustom (callback) {
    if (this.express && this.express.webClient) {
      this.express.webClient.stop()
    }
    return callback(null)
  }

  createAgent (req, res, next) {
    let msg, name, rfc, sets

    if (!req.principal) {
      return next(new Error('No principal for createAgent()'))
    }

    if (!req.principal.id) {
      const ps = JSON.stringify(req.principal)
      msg = `No id in principal ${ps} for createAgent()`
      return next(new Error())
    }

    if (!req.user) {
      return next(new Error('No user for createAgent()'))
    }

    if (!req.user.id) {
      const us = JSON.stringify(req.user)
      return next(new Error(`No req.user.id in user ${us} for createAgent()`))
    }

    if (req.body.performance != null) {
      if (!_.isObject(req.body.performance)) {
        return next(new Error('performance must be an object'))
      }
      for (name in req.body.performance) {
        const direction = req.body.performance[name]
        if (!['maximize', 'minimize'].includes(direction)) {
          msg = `performance metric ${name} must be ` +
            "either 'maximize' or 'minimize'"
          return next(new Error(msg))
        }
      }
    }

    const props = {
      userID: req.user.id,
      name: req.body.name,
      description: req.body.description
    }

    if (req.body.performance != null) {
      props.performance = req.body.performance
    }

    // Validate by creating a new RFC

    const params = _.pick(req.body, 'inputs', 'outputs', 'rules', 'parsed_rules')

    for (name in params.inputs) {
      sets = params.inputs[name]
      debug(`name = ${name}`)
      debug(`sets = ${JSON.stringify(sets)}`)
      if (_.isArray(sets)) {
        debug("It's an array")
        params.inputs[name] = this.expandRangeToSets(sets, name)
      }
    }

    for (name in params.outputs) {
      sets = params.outputs[name]
      debug(`name = ${name}`)
      debug(`sets = ${JSON.stringify(sets)}`)
      if (_.isArray(sets)) {
        debug("It's an array")
        params.outputs[name] = this.expandRangeToSets(sets, name)
      }
    }

    try {
      rfc = new RFC(params)
    } catch (error) {
      // validation errors indicate a problem on the client side
      const err = error
      return next(new HTTPError(err.message, 400))
    }

    // Save what's in the fuzzy controller

    props.properties = rfc.toJSON()

    return Agent.create(props, (err, fc) => {
      if (err) {
        return next(err)
      } else {
        const results = this.agentToResults(fc)
        if (!res.headersSent) {
          res.set('Location', this.makeURL(`/agent/${fc.id}`))
        }
        return res.json(results)
      }
    })
  }

  listAgents (req, res, next) {
    debug(`Getting agents for user ${req.user.id}`)
    return async.parallel([
      callback => req.user.getAgents(callback),
      callback => req.user.getDeletedAgents(callback)
    ], (err, results) => {
      if (err) {
        debug(`Error getting agents for user ${req.user.id}: ${err}`)
        return next(err)
      } else {
        const [agents, deletedAgents] = Array.from(results)
        debug(`Got ${agents.length} agents for user ${req.user.id}`)
        const lu = _.max(_.map(agents, 'updatedAt'))
        const ld = _.max(_.map(deletedAgents, 'deletedAt'))
        const uc = req.user.createdAt
        const lm = _.max([lu, ld, uc])
        res.set('Last-Modified', (new Date(lm)).toISOString())
        const brief = _.map(agents, agent => _.pick(agent, 'id', 'name'))
        debug(brief)
        return res.json(brief)
      }
    })
  }

  getAgent (req, res, next) {
    debug(`Getting agent ${req.agent.id}`)
    const results = this.agentToResults(req.agent)
    const lastModified = (new Date(req.agent.updatedAt)).toUTCString()
    res.set('Last-Modified', lastModified)
    return res.json(results)
  }

  putAgent (req, res, next) {
    let name, rfc, sets
    if (req.body.performance != null) {
      if (!_.isObject(req.body.performance)) {
        return next(new Error('performance must be an object'))
      }
      for (name in req.body.performance) {
        const direction = req.body.performance[name]
        if (!['maximize', 'minimize'].includes(direction)) {
          const msg = `performance metric ${name} must be ` +
            "either 'maximize' or 'minimize'"
          return next(new Error(msg))
        }
      }
    }

    if (req.body.name != null) {
      if (!_.isString(req.body.name)) {
        return next(new Error('Name must be a string'))
      }
    }

    if (req.body.description != null) {
      if (!_.isString(req.body.description)) {
        return next(new Error('Description must be a string'))
      }
    }

    const props = {
      name: req.body.name,
      description: req.body.description,
      performance: req.body.performance
    }

    const params = _.pick(req.body, 'inputs', 'outputs', 'rules', 'parsed_rules')

    debug(`params = ${JSON.stringify(params)}`)

    for (name in params.inputs) {
      sets = params.inputs[name]
      debug(`name = ${name}`)
      debug(`sets = ${JSON.stringify(sets)}`)
      if (_.isArray(sets)) {
        debug("It's an array")
        params.inputs[name] = this.expandRangeToSets(sets, name, __guard__(__guard__(req.agent != null ? req.agent.properties : undefined, x1 => x1.inputs), x => x[name]))
      }
    }

    for (name in params.outputs) {
      sets = params.outputs[name]
      debug(`name = ${name}`)
      debug(`sets = ${JSON.stringify(sets)}`)
      if (_.isArray(sets)) {
        debug("It's an array")
        params.outputs[name] = this.expandRangeToSets(sets, name, __guard__(__guard__(req.agent != null ? req.agent.properties : undefined, x3 => x3.outputs), x2 => x2[name]))
      }
    }

    debug('Done expanding')
    debug(`params = ${JSON.stringify(params)}`)

    // Validate by creating a new RFC

    try {
      rfc = new RFC(params)
    } catch (error) {
      // Errors here indicate invalid client content
      const err = error
      return next(new HTTPError(err.message, 400))
    }

    props.properties = rfc.toJSON()

    return req.agent.update(props, (err, updated) => {
      if (err) {
        return next(err)
      } else {
        return res.json(this.agentToResults(updated))
      }
    })
  }

  deleteAgent (req, res, next) {
    return req.agent.del((err) => {
      if (err) {
        return next(err)
      } else {
        delete req.agent
        return res.json({message: 'OK'})
      }
    })
  }

  listAgentVersions (req, res, next) {
    return req.agent.getVersions((err, versions) => {
      if (err) {
        return next(err)
      } else {
        const ids = _.map(versions, 'id')
        return res.json(ids)
      }
    })
  }

  getVersion (req, res, next) {
    const lastModified = (new Date(req.version.createdAt)).toUTCString()
    res.set('Last-Modified', lastModified)
    // Versions are immutable so set the Expires date in the far future
    const expires = (new Date(Date.now() + (10 * YEAR))).toUTCString()
    res.set('Expires', expires)
    const results = this.versionToResults(req.version)
    return res.json(results)
  }

  getEvaluationLog (req, res, next) {
    return AgentVersion.get(req.evaluationLog.versionID, (err, version) => {
      if (err) {
        return next(err)
      } else {
        const payload = _.clone(req.evaluationLog)
        const { rules } = version.properties
        const indices = req.evaluationLog.rules
        payload.rules_text = this.pickRules(rules, indices)
        const lm = (new Date(req.evaluationLog.createdAt)).toUTCString()
        res.set('Last-Modified', lm)
        // Evaluation logs are immutable so set the Expires date
        // in the far future
        const expires = (new Date(Date.now() + (10 * YEAR))).toUTCString()
        res.set('Expires', expires)
        return res.json(payload)
      }
    })
  }

  getFeedback (req, res, next) {
    const el = req.evaluationLog
    const client = req.app.statsClient
    return client.getFeedback(el.reqID, (err, feedbacks) => {
      if (err) {
        return next(err)
      } else {
        return res.json(feedbacks)
      }
    })
  }

  postFeedback (req, res, next) {
    const el = req.evaluationLog
    const client = req.app.statsClient
    const data = req.body

    debug(`Data = ${js(data)}`)

    // XXX: Lost evaluation log. Just spoot it back out!

    if ((req.agent == null)) {
      const faked = {
        id: req.id,
        evaluationLog: req.evaluationLogID,
        data,
        createdAt: (new Date()).toISOString()
      }
      res.json(faked)
      return
    }

    if (_.isNull(data) || _.isArray(data) || !_.isObject(data) || (_.keys(data).length === 0) || !_.every(_.keys(data), _.isString) || !_.every(_.keys(data), k => k.length > 0) || !_.every(_.values(data), _.isNumber)) {
      return next(new HTTPError('Body must be object mapping performance to numbers', 400))
    }

    // Add a metric to our agent if it doesn't already exist

    const ensureMetric = function (name, callback) {
      if (_.has((req.agent.performance != null), name)) {
        return callback(null)
      } else if (_.has(req.agent.properties.outputs, name)) {
        return callback(null)
      } else {
        if ((req.agent.performance == null)) {
          req.agent.performance = {}
        }
        req.agent.performance[name] = 'minimize'
        return req.agent.save(err => callback(err))
      }
    }

    return async.waterfall([
      callback => async.each(_.keys(req.body), ensureMetric, callback),
      callback => client.postFeedback(el.reqID, data, callback)
    ], (err, feedback) => {
      if (err) {
        return next(err)
      } else {
        setImmediate(() => {
          const outputSets = req.agent.properties.outputs
          const fb = req.body
          const lc = req.app.learningClient
          const def = req.agent.properties
          const inputs = el.input
          if (_.every(_.keys(fb), key => _.has(outputSets, key))) {
            return lc.optimize(def, inputs, fb, (err, newDef) => {
              if (err) {
                return console.error(err)
              } else {
                req.agent.properties = newDef
                return req.agent.save((err) => {
                  if (err) {
                    return console.error(err)
                  }
                })
              }
            })
          }
        })
        return res.json(feedback)
      }
    })
  }

  getAppVersion (req, res, next) {
    return res.json({name: 'api', version, controllerVersion: RFC.version})
  }

  agentToResults (agent) {
    const results = _.clone(agent)
    results.inputs = results.properties.inputs
    results.outputs = results.properties.outputs
    results.rules = results.properties.rules
    if (agent.properties.parsed_rules != null) {
      results.parsed_rules = results.properties.parsed_rules
    } else {
      const rfc = new RFC(agent.properties)
      const json = rfc.toJSON()
      results.parsed_rules = json.parsed_rules
    }
    delete results.properties
    return results
  }

  versionToResults (version) {
    return this.agentToResults(version)
  }

  requireBody (req, res, next) {
    if (req.body != null) {
      return next()
    } else {
      return next(new HTTPError('No JSON payload in request', 400))
    }
  }

  userID (req, res, next, id) {
    return User.get(id, (err, user) => {
      if (err) {
        if (err.name === 'NoSuchThingError') {
          return DeletedUser.get(id, (err2, deleted) => {
            if (err2) {
              return next(err)
            } else {
              return next(new HTTPError(`User ${id} deleted`, 410))
            }
          })
        } else {
          return next(err)
        }
      } else {
        req.user = user
        return next()
      }
    })
  }

  agentID (req, res, next, id) {
    debug(`Getting agent ${id}`)

    // try various transformations on the ID to see if we have it

    const tryAgent = function (transformer, callback) {
      const tid = transformer(id)
      debug(`Trying to find agent with transformed id ${tid}`)
      return Agent.get(tid, (err, agent) => {
        if (err) {
          if (err.name === 'NoSuchThingError') {
            debug(`Couldn't find agent with transformed id ${tid}: ${err}`)
            return callback(null, false)
          } else {
            debug(`Couldn't get agent with transformed id ${tid}: ${err}`)
            return callback(err)
          }
        } else {
          debug(`Found agent with transformed id ${tid}`)
          req.agent = agent
          return callback(null, true)
        }
      })
    }

    const tryDeleted = function (transformer, callback) {
      const tid = transformer(id)
      debug(`Trying to find deleted agent with transformed id ${tid}`)
      return DeletedAgent.get(tid, (err, deleted) => {
        if (err) {
          if (err.name === 'NoSuchThingError') {
            debug(`Couldn't find deleted agent with transformed id ${tid}: ${err}`)
            return callback(null, false)
          } else {
            debug(`Couldn't get deleted agent with transformed id ${tid}: ${err}`)
            return callback(err)
          }
        } else {
          debug(`Found deleted agent with transformed id ${tid}`)
          return callback(null, true)
        }
      })
    }

    const transformers = [_.identity, _.toUpper, _.toLower]

    return async.detectSeries(transformers, tryAgent, (err, passed) => {
      if (err) {
        return next(err)
      } else if (passed != null) {
        return next()
      } else {
        return async.detectSeries(transformers, tryDeleted, (err, passed) => {
          if (err != null) {
            return next(err)
          } else if (passed != null) {
            return next(new HTTPError(`Agent with ID ${id} was deleted`, 410))
          } else {
            return next(new HTTPError(`No agent found for ID ${id}`, 404))
          }
        })
      }
    })
  }

  versionID (req, res, next, id) {
    return AgentVersion.get(id, (err, version) => {
      if (err) {
        if (err.name === 'NoSuchThingError') {
          return DeletedAgentVersion.get(id, (err2, deleted) => {
            if (err2) {
              return next(err)
            } else {
              return next(new HTTPError(`Version ${id} deleted`, 410))
            }
          })
        } else {
          return next(err)
        }
      } else {
        req.version = version
        return next()
      }
    })
  }

  evaluationID (req, res, next, id) {
    req.evaluationLogID = id
    return next()
  }

  reqEvaluationLog (req, res, next) {
    assert.ok(req.app.statsClient)
    debug(`Getting evaluation ${req.evaluationLogID}`)
    return req.app.statsClient.getEvaluationLog(req.evaluationLogID, (err, body) => {
      debug(`Results: ${js(err)}`)
      debug(`Data: ${js(body)}`)
      if (err) {
        debug(err)
        // XXX: We fake it if it doesn't exist
        if (err.statusCode === 404) {
          req.evaluationLog = {
            reqID: req.evaluationLogID,
            userID: req.principal.id
          }
          debug(js(req.evaluationLog))
          return next()
        } else {
          return next(err)
        }
      } else {
        req.evaluationLog = body
        return next()
      }
    })
  }

  evaluationLogAgent (req, res, next) {
    assert.ok(req.evaluationLog)
    // XXX: Fake it for non-existent evaluation log
    if ((req.evaluationLog.agentID == null)) {
      req.agent = null
      return next()
    } else {
      return Agent.get(req.evaluationLog.agentID, (err, body) => {
        if (err) {
          return next(err)
        } else {
          req.agent = body
          return next()
        }
      })
    }
  }

  authc (req, res, next) {
    return this.bearerToken(req, (err, tokenString) => {
      if (err) {
        return next(err)
      } else {
        return async.waterfall([
          callback =>
            AccessToken.get(tokenString, (err, token) => {
              if (err != null) {
                if (err.name === 'NoSuchThingError') {
                  return callback(new HTTPError('Invalid access token', 401))
                } else {
                  return callback(err)
                }
              } else {
                return callback(null, token)
              }
            }),
          function (token, callback) {
            req.token = token
            if (!token.userID) {
              return callback(new HTTPError(`No userID for token ${tokenString}`, 401))
            } else {
              return User.get(token.userID, callback)
            }
          }
        ], (err, user) => {
          if (err) {
            return next(err)
          } else {
            req.principal = user
            return next()
          }
        })
      }
    })
  }

  principalAsUser (req, res, next) {
    req.user = req.principal
    return next()
  }

  userIsPrincipal (req, res, next) {
    if ((req.user != null ? req.user.id : undefined) === (req.principal != null ? req.principal.id : undefined)) {
      return next()
    } else {
      return next(new HTTPError('Not yours!', 403))
    }
  }

  userOwnsAgent (req, res, next) {
    if ((req.user != null ? req.user.id : undefined) === (req.agent != null ? req.agent.userID : undefined)) {
      return next()
    } else {
      return next(new HTTPError('Not yours!', 403))
    }
  }

  principalOwnsAgent (req, res, next) {
    if ((req.principal != null ? req.principal.id : undefined) === (req.agent != null ? req.agent.userID : undefined)) {
      return next()
    } else {
      return next(new HTTPError('Not yours!', 403))
    }
  }

  principalOwnsVersion (req, res, next) {
    if ((req.principal != null ? req.principal.id : undefined) === (req.version != null ? req.version.userID : undefined)) {
      return next()
    } else {
      return next(new HTTPError('Not yours!', 403))
    }
  }

  principalOwnsEvaluationLog (req, res, next) {
    if ((req.principal != null ? req.principal.id : undefined) === (req.evaluationLog != null ? req.evaluationLog.userID : undefined)) {
      return next()
    } else {
      return next(new HTTPError('Not yours!', 403))
    }
  }

  ownerAsUser (req, res, next) {
    return User.get(req.agent != null ? req.agent.userID : undefined, (err, user) => {
      if (err) {
        return next(err)
      } else {
        req.user = user
        return next()
      }
    })
  }

  versionOfAgent (req, res, next) {
    if ((req.version != null ? req.version.versionOf : undefined) === (req.agent != null ? req.agent.id : undefined)) {
      return next()
    } else {
      return next(new HTTPError("Version doesn't belong to agent", 403))
    }
  }

  // FIXME: I had to dike out the check for the API limit because calls to the
  // stats server were timing out.

  ignoreAPILimit (req, res, next) {
    return next()
  }

  underAPILimit (req, res, next) {
    const { user } = req
    const now = new Date()
    const year = now.getUTCFullYear()
    const month = now.getUTCMonth() + 1

    let limit = null

    return async.parallel([
      callback => req.app.planClient.getPlan((user.plan || 'free'), callback),
      callback => req.app.statsClient.getUserUsage(user.id, year, month, callback)
    ], (err, results) => {
      if (err) {
        process.stderr.write(JSON.stringify(err))
        setImmediate(() => { // NOTE: On internal error, let the request go through!
          const msg = `Error checking API limit: ${err.message}`
          return this.slackMessage('error', msg, ':bomb:', (err) => {
            if (err) {
              return console.error(err)
            }
          })
        })
        return next()
      } else {
        const [plan, cnt] = Array.from(results)
        limit = plan.apiLimit

        if (_.isNull(limit)) {
          return next()
        } else {
          let nextMonth
          if (month === 12) {
            nextMonth = new Date(year + 1, 1)
          } else {
            // 'month' let starts with 1, Date constructor arg starts with 0
            nextMonth = new Date(year, (month - 1) + 1)
          }
          const secondsLeft = Math.round((nextMonth.getTime() - now.getTime()) / 1000)

          req.apiLimit = limit
          req.apiCount = cnt
          req.secondsLeft = secondsLeft

          res.set('X-Rate-Limit-Limit', limit)
          res.set('X-Rate-Limit-Remaining', Math.max(limit - cnt, 0))
          res.set('X-Rate-Limit-Reset', secondsLeft)

          if (cnt >= limit) {
            res.set('Retry-After', nextMonth.toUTCString())
            return next(new HTTPError(`${cnt} API calls; over API limit ${limit}`, 429))
          } else {
            return next()
          }
        }
      }
    })
  }

  saveEvaluationLog (client, user, agent, id, input, results, callback) {
    const props = {
      reqID: id,
      userID: user.id,
      agentID: agent.id,
      versionID: agent.latestVersion,
      input,
      fuzzified: results.fuzzified,
      rules: results.rules,
      inferred: results.inferred,
      clipped: results.clipped,
      combined: results.combined,
      centroid: results.centroid,
      crisp: _.pick(results.crisp, _.filter(_.keys(results.crisp), key => !_.isObject(results.crisp[key])))
    }

    debug('Evaluation log props')
    debug(props)

    return client.newEvaluationLog(props, callback)
  }

  defaultSetNames (count, dimensionName) {
    // TODO: use dimension name for some sets, like
    // defaultSetNames(4, "temperature") -> ["cold", "cool", "warm", "hot"]

    switch (count) {
      case 1:
        return ['medium']
      case 2:
        return ['low', 'high']
      case 3:
        return ['low', 'medium', 'high']
      case 4:
        return ['very low', 'low', 'high', 'very high']
      case 5:
        return ['very low', 'low', 'medium', 'high', 'very high']
      case 6:
        return ['very low', 'low', 'medium low', 'medium high', 'high', 'very high']
      case 7:
        return ['very low', 'low', 'medium low', 'medium',
          'medium high', 'high', 'very high']
      case 8:
        return ['very very low', 'very low', 'low', 'medium low',
          'medium high', 'high', 'very high', 'very very high']
      case 9:
        return ['very very low', 'very low', 'low', 'medium low', 'medium',
          'medium high', 'high', 'very high', 'very very high']
      default:
        return _.map(__range__(1, count - 1, true), i => `set ${i}`)
    }
  }

  expandRangeToSets (range, dimensionName, existing) {
    const DEFAULT_COUNT = 5

    debug(`range = ${JSON.stringify(range)}`)
    debug(`dimensionName = ${dimensionName}`)
    debug(`existing = ${JSON.stringify(existing)}`)

    if (!_.isArray(range) || (range.length < 2) || (range.length > 3)) {
      // We don't handle this. Let fuzzy-controller try to understand it
      return range
    } else {
      let names
      const [low, high] = Array.from(range)
      let [,, count] = range
      const sets = {}

      if ((existing != null) && ((count == null) || (_.keys(existing).count === count))) {
        debug('Using existing names')
        count = _.keys(existing).length
        debug(`count = ${JSON.stringify(count)}`)
        const sorted = _.sortBy(_.toPairs(existing), x => x[1][0])
        debug(`sorted = ${JSON.stringify(sorted)}`)
        names = _.map(sorted, x => x[0])
        debug(`names = ${JSON.stringify(names)}`)
      } else {
        if ((count == null)) {
          count = DEFAULT_COUNT
        }
        names = this.defaultSetNames(count, dimensionName)
      }

      const interval = (high - low) / (count - 1)

      for (let i = 0, end = count - 1, asc = end >= 0; asc ? i <= end : i >= end; asc ? i++ : i--) {
        if (i === 0) { // left shoulder
          sets[names[i]] = [low, low + interval]
        } else if (i === (count - 1)) { // left shoulder
          sets[names[i]] = [high - interval, high]
        } else {
          sets[names[i]] = [
            low + ((i - 1) * interval),
            low + (i * interval),
            low + ((i + 1) * interval)
          ]
        }
      }

      return sets
    }
  }

  postAgent (req, res, next) {
    let err, inputs, needle
    const client = req.app.evaluatorClient

    // Do we return meta data?

    let { meta } = req.query

    if ((meta != null) && (needle = meta.toLowerCase(), ['true', 'yes', 'on', '1'].includes(needle))) {
      meta = 'meta'
    }

    const validateInputs = function (inputs, defs) {
      assert.ok(_.isObject(inputs, `${inputs} is not an object.`))

      return _.each(inputs, (value, name) => {
        assert.ok(_.has(defs, name), `Unrecognized argument '${name}'.`)
        assert.ok(_.isNumber(value), `'${name}' argument is not a number.`)
      })
    }

    const evalInputs = function (inputs, callback) {
      const id = uuid.v1()
      return runEval(id, req.agent.properties, inputs, callback)
    }

    const runEval = (id, properties, inputs, callback) => {
      debug(`ID = ${id}`)
      return client.evaluate(properties, inputs, (err, results) => {
        if (err) {
          return callback(err)
        } else {
          const cli = req.app.statsClient
          const ag = req.agent
          const u = req.user
          setImmediate(this.saveEvaluationLog, cli, u, ag, id, inputs, results, (err, results) => {
            if (err != null) {
              let extra
              req.log.error({err}, 'Error saving evaluation log')
              if (err.body != null) {
                const parsed = JSON.parse(err.body)
                extra = parsed.message
              } else {
                extra = null
              }
              const msg = `${err.name}: ${err.message} ` +
                `(saving evaluation log for user ${u.email} ` +
                `evaluation ${id} extra '${extra}')`
              return this.slackMessage('error', msg, ':bomb:', (err) => {
                if (err) {
                  return req.log.error({err}, 'Error posting to Slack')
                }
              })
            }
          })

          if (meta != null) {
            // Blech
            results.crisp[meta] = {
              fuzzified: results.fuzzified,
              rules: results.rules,
              rules_text: this.pickRules(req.agent.properties.rules, results.rules),
              inferred: results.inferred,
              clipped: results.clipped,
              combined: results.combined,
              centroid: results.centroid,
              reqID: id
            }
          }
          return callback(null, results)
        }
      })
    }

    if (_.isArray(req.body)) {
      // Limit the batch size to something reasonable

      if (req.body.length > MAX_BATCH_SIZE) {
        const msg = `Batch size ${req.body.length} ` +
          `exceeds maximum size ${MAX_BATCH_SIZE}`
        return next(new HTTPError(msg, 413))
      }

      for (inputs of Array.from(req.body)) {
        try {
          validateInputs(inputs, req.agent.properties.inputs)
        } catch (error) {
          err = error
          return next(new HTTPError(err.message, 400))
        }
      }
      return async.map(req.body, evalInputs, (err, resultses) => {
        if (err) {
          return next(err)
        } else {
          const crisps = _.map(resultses, 'crisp')
          return res.json(crisps)
        }
      })
    } else if (_.isObject(req.body)) {
      try {
        validateInputs(req.body, req.agent.properties.inputs)
      } catch (error1) {
        err = error1
        return next(new HTTPError(err.message, 400))
      }
      const id = uuid.v1()
      return runEval(id, req.agent.properties, req.body, (err, results) => {
        if (err) {
          return next(err)
        } else {
          res.set('X-Evaluation-ID', id)
          const outputs = _.cloneDeep(results.crisp)
          return res.json(outputs)
        }
      })
    } else {
      return next(new HTTPError(`Invalid body for evaluation: ${req.body}`, 400))
    }
  }

  makeURL (rel) {
    if (this.config.urlPrefix != null) {
      return `${this.config.urlPrefix}${rel}`
    } else {
      let def, host
      if ((this.config.key != null) && (this.config.port === 443)) {
        def = true
      } else if ((this.config.key == null) && (this.config.port === 80)) {
        def = true
      } else {
        def = false
      }

      if (def) {
        host = this.config.hostname
      } else {
        host = `${this.config.hostname}:${this.config.port}`
      }

      const props = {
        protocol: (this.config.key != null) ? 'https:' : 'http:',
        host,
        pathname: rel
      }

      return url.format(props)
    }
  }

  pickRules (rules, indices) {
    return _.values(_.pick(rules, indices))
  }

  getRoot (req, res, next) {
    res.set('Content-Type', 'text/html')
    return res.send(`\
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="0;URL='${this.config.docsURL}'" />
    <title>API root</title>
  </head>
  <body>
    <h1>API root</h1>
    <p>
      There's nothing interesting here.
      Try <a href='${this.config.docsURL}'>${this.config.docsURL}</a>.
    </p>
  </body>
</html>\
`
    )
  }

  getLive (req, res, next) {
    return res.json({status: 'OK'})
  }

  getReady (req, res, next) {
    if ((this.db == null)) {
      return next(HTTPError('Database not connected', 500))
    }

    return async.parallel([
      callback => {
        // Check that we can contact the database
        return this.db.save('server-ready', 'api', 1, callback)
      },
      callback => req.app.statsClient.version(callback),
      callback => req.app.planClient.version(callback),
      callback => req.app.evaluatorClient.version(callback),
      callback => req.app.learningClient.version(callback)
    ], (err) => {
      if (err != null) {
        debug(`${err.name}: ${err.message} in /ready`)
        return next(err)
      } else {
        return res.json({status: 'OK'})
      }
    })
  }
}

module.exports = APIServer

function __guard__ (value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined
}
function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}
