// planclient.js
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const MicroserviceClient = require('@fuzzy-ai/microservice-client')

class EvaluatorClient extends MicroserviceClient {
  evaluate (properties, inputs, callback) {
    return this.post('/evaluate', {properties, inputs}, callback)
  }

  version (callback) {
    return this.get('/version', callback)
  }
}

module.exports = EvaluatorClient
