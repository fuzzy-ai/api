// deletedagentversion.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const db = require('databank')

const timestamp = require('./timestamp')

const DeletedAgentVersion = db.DatabankObject.subClass('DeletedAgentVersion')

DeletedAgentVersion.schema = {
  pkey: 'id',
  fields: [
    'userID',
    'versionOf',
    'createdAt',
    'updatedAt',
    'deletedAt'
  ],
  indices: ['userID', 'versionOf']
}

DeletedAgentVersion.beforeCreate = function (props, callback) {
  try {
    assert(_.isString(props.id), "No 'id' in new DeletedAgentVersion")
    assert(_.isString(props.versionOf), "No 'versionOf' in new DeletedAgentVersion")
    assert(_.isString(props.userID), "No 'userID' in new DeletedAgentVersion")
    assert(_.isString(props.createdAt), "No 'createdAt' in new DeletedAgentVersion")
  } catch (err) {
    callback(err, null)
    return
  }

  props.deletedAt = timestamp()

  return callback(null, props)
}

DeletedAgentVersion.fromVersion = function (version, callback) {
  const props = _.pick(version, 'id', 'userID', 'versionOf', 'createdAt')

  return DeletedAgentVersion.create(props, callback)
}

module.exports = DeletedAgentVersion
