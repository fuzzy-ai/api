// timestamp.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const timestamp = () => (new Date()).toISOString()

module.exports = timestamp
