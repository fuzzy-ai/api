// agent.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')

const async = require('async')
const _ = require('lodash')
const db = require('databank')

const timestamp = require('./timestamp')
const uniqueID = require('./uniqueid')
const AgentVersion = require('./agentversion')
const DeletedAgent = require('./deletedagent')

const Agent = db.DatabankObject.subClass('Agent')

Agent.schema = {
  pkey: 'id',
  fields: [
    'userID',
    'latestVersion',
    'name',
    'description',
    'createdAt',
    'updatedAt',
    'performance'
  ],
  indices: ['userID']
}

Agent.beforeCreate = function (props, callback) {
  try {
    assert(_.isString(props.userID), 'Agent must have a userID')
    assert(_.isObject(props.properties), 'Agent must have properties property')
    assert(_.isObject(props.properties.inputs),
      'Agent properties property must have inputs property')
    assert(_.isObject(props.properties.outputs),
      'Agent properties property must have outputs property')
    assert(_.isArray(props.properties.rules),
      'Agent properties property must have rules property')
  } catch (error) {
    const err = error
    return callback(err)
  }

  props.createdAt = (props.updatedAt = timestamp())

  return async.waterfall([
    callback => uniqueID(callback),
    function (id, callback) {
      props.id = uniqueID()

      const ver = {
        versionOf: props.id,
        userID: props.userID,
        properties: props.properties
      }

      return AgentVersion.create(ver, callback)
    }
  ], (err, created) => {
    if (err) {
      return callback(err)
    } else {
      delete props.properties
      props.latestVersion = created.id
      return callback(null, props)
    }
  })
}

Agent.prototype.beforeUpdate = function (props, callback) {
  try {
    assert(_.isObject(props.properties), 'Agent must have properties property')
    assert(_.isObject(props.properties.inputs),
      'Agent properties property must have inputs property')
    assert(_.isObject(props.properties.outputs),
      'Agent properties property must have outputs property')
    assert(_.isArray(props.properties.rules),
      'Agent properties property must have rules property')
  } catch (error) {
    const err = error
    return callback(err)
  }

  const ver = {
    versionOf: this.id,
    userID: this.userID,
    properties: props.properties
  }

  return AgentVersion.create(ver, (err, created) => {
    if (err) {
      return callback(err)
    } else {
      delete props.properties
      props.latestVersion = created.id
      props.updatedAt = timestamp()
      return callback(null, props)
    }
  })
}

Agent.prototype.beforeSave = function (callback) {
  try {
    assert(_.isString(this.userID), 'Agent must have a userID')
    assert(_.isObject(this.properties), 'Agent must have properties property')
    assert(_.isObject(this.properties.inputs),
      'Agent properties property must have inputs property')
    assert(_.isObject(this.properties.outputs),
      'Agent properties property must have outputs property')
    assert(_.isArray(this.properties.rules),
      'Agent properties property must have rules property')
  } catch (err) {
    return callback(err)
  }

  this.updatedAt = timestamp()

  return async.waterfall([
    callback => {
      if (this.id != null) {
        this.createdAt = this.updatedAt
        return callback(null, this.id)
      } else {
        return uniqueID(callback)
      }
    },
    (id, callback) => {
      this.id = id
      const ver = {
        versionOf: this.id,
        userID: this.userID,
        properties: this.properties
      }
      return AgentVersion.create(ver, callback)
    },
    (version, callback) => {
      delete this.properties
      this.latestVersion = version.id
      return callback(null)
    }
  ], callback)
}

const MAX_BACKOFF = 15
const TIME_SLOT = 100

Agent.prototype.afterGet = function (callback) {
  if (this.latestVersion != null) {
    const getVersion = callback => {
      return AgentVersion.get(this.latestVersion, callback)
    }
    return async.retry({
      times: MAX_BACKOFF,
      interval (retries) {
        const c = Math.min(MAX_BACKOFF, retries)
        return _.random(0, Math.pow(2, c - 1)) * TIME_SLOT
      }
    }, getVersion, (err, ver) => {
      if (err) {
        return callback(err)
      } else {
        this.properties = ver.properties
        return callback(null)
      }
    })
  } else {
    return callback(null)
  }
}

Agent.prototype.afterCreate = Agent.prototype.afterGet
Agent.prototype.afterUpdate = Agent.prototype.afterGet
Agent.prototype.afterSave = Agent.prototype.afterGet

Agent.prototype.getVersions = function (callback) {
  return AgentVersion.search({versionOf: this.id}, callback)
}

Agent.prototype.afterDel = function (callback) {
  return async.waterfall([
    callback => {
      // Archive the data for auditing
      return DeletedAgent.fromController(this, callback)
    },
    (deleted, callback) => {
      return this.getVersions(callback)
    },
    (versions, callback) => {
      // Delete the versions, too
      const deleteVersion = (version, callback) => {
        return version.del(callback)
      }
      return async.eachLimit(versions, 16, deleteVersion, callback)
    }
  ], err => callback(err))
}

module.exports = Agent
