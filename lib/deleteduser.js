// deleteduser.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const db = require('databank')

const timestamp = require('./timestamp')

const DeletedUser = db.DatabankObject.subClass('DeletedUser')

DeletedUser.schema = {
  pkey: 'id',
  fields: [
    'email',
    'createdAt',
    'updatedAt',
    'deletedAt'
  ]
}

DeletedUser.beforeCreate = function (props, callback) {
  try {
    assert(_.isString(props.id), "No 'id' in new DeletedUser")
    assert(_.isString(props.email), "No 'userID' in new DeletedUser")
    assert(_.isString(props.createdAt), "No 'createdAt' in new DeletedUser")
    assert(_.isString(props.updatedAt), "No 'updatedAt' in new DeletedUser")
  } catch (err) {
    callback(err, null)
    return
  }

  props.deletedAt = timestamp()

  return callback(null, props)
}

DeletedUser.fromUser = function (user, callback) {
  const props = _.pick(user, 'id', 'email', 'createdAt', 'updatedAt')

  return DeletedUser.create(props, callback)
}

module.exports = DeletedUser
