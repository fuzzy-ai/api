// agentversion.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const db = require('databank')

const timestamp = require('./timestamp')
const uniqueID = require('./uniqueid')
const DeletedAgentVersion = require('./deletedagentversion')

const AgentVersion = db.DatabankObject.subClass('AgentVersion')

AgentVersion.schema = {
  pkey: 'id',
  fields: [
    'userID',
    'versionOf',
    'createdAt',
    'properties'
  ],
  indices: ['userID', 'versionOf']
}

AgentVersion.beforeCreate = function (props, callback) {
  try {
    assert(_.isString(props.userID), 'AgentVersion must have a userID property')
    assert(_.isString(props.versionOf), 'AgentVersion must have a versionOf property')
    assert(_.isObject(props.properties), 'AgentVersion must have a properties property')
    assert(_.isObject(props.properties.inputs), 'AgentVersion properties property must have an inputs property')
    assert(_.isObject(props.properties.outputs), 'AgentVersion properties property must have an outputs property')
    assert(_.isArray(props.properties.rules), 'AgentVersion properties property must have a rules property')
  } catch (error) {
    const err = error
    return callback(err)
  }

  props.createdAt = timestamp()

  return uniqueID((err, id) => {
    if (err) {
      callback(err)
    } else {}
    props.id = id
    return callback(null, props)
  })
}

AgentVersion.prototype.beforeUpdate = (props, callback) => callback(new Error('AgentVersion is immutable'), null)

AgentVersion.prototype.afterDel = function (callback) {
  return DeletedAgentVersion.fromVersion(this, err => callback(err))
}

module.exports = AgentVersion
