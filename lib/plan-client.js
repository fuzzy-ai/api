// planclient.js
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const MicroserviceClient = require('@fuzzy-ai/microservice-client')

class PlanClient extends MicroserviceClient {
  getPlans (callback) {
    return this.get('/plans', callback)
  }

  getPlan (stripeId, callback) {
    return this.get(`/plan/${stripeId}`, callback)
  }

  version (callback) {
    return this.get('/version', callback)
  }
}

module.exports = PlanClient
