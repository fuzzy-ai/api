// statsclient.js
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const qs = require('querystring')
const uuidMeta = require('uuid-meta')

const MicroserviceClient = require('@fuzzy-ai/microservice-client')

const RETRY = 60000
const MAX_RETRIES = 40

const lpad = function (value, padding) {
  let zeroes = '0'
  for (let i = 1, end = padding, asc = end >= 1; asc ? i <= end : i >= end; asc ? i++ : i--) { zeroes += '0' }

  return (zeroes + value).slice(padding * -1)
}

class StatsClient extends MicroserviceClient {
  getAgentStats (agentID, year, month, callback) {
    return this.get(`/agent/${agentID}/${lpad(year, 4)}/${lpad(month, 2)}`, callback)
  }

  getUserStats (userID, year, month, callback) {
    return this.get(`/user/${userID}/${lpad(year, 4)}/${lpad(month, 2)}`, callback)
  }

  getUserLogs (userID, offset, limit, callback) {
    if ((callback == null)) {
      callback = limit
      limit = 20
    }

    if ((callback == null)) {
      callback = offset
      offset = 0
    }

    const params = qs.stringify({offset, limit})

    return this.get(`/user/${userID}/stream?${params}`, callback)
  }

  getAgentLogs (agentID, offset, limit, callback) {
    if ((callback == null)) {
      callback = limit
      limit = 20
    }

    if ((callback == null)) {
      callback = offset
      offset = 0
    }

    const params = qs.stringify({offset, limit})

    return this.get(`/agent/${agentID}/stream?${params}`, callback)
  }

  getEvaluationLog (evaluationID, callback) {
    let retries = 0
    const handler = function (err, body) {
      if (err) {
        // Special case for very recent UUIDs
        if (err.statusCode === 404) {
          const meta = uuidMeta(evaluationID)
          if ((meta.version === 1) && ((Date.now() - meta.time_unix) < RETRY) && (retries < MAX_RETRIES)) {
            // Try again in a second
            retries += 1
            return setTimeout(() => {
              return this.get(`/evaluation/${evaluationID}`, handler
                , 500)
            })
          } else {
            return callback(err)
          }
        } else {
          return callback(err)
        }
      } else {
        return callback(null, body)
      }
    }

    return this.get(`/evaluation/${evaluationID}`, handler)
  }

  postFeedback (evaluationID, body, callback) {
    return this.post(`/evaluation/${evaluationID}/feedback`, body, callback)
  }

  getFeedback (evaluationID, callback) {
    return this.get(`/evaluation/${evaluationID}/feedback`, callback)
  }

  newEvaluationLog (body, callback) {
    return this.post('/evaluation', body, callback)
  }

  getUserUsage (userID, year, month, callback) {
    return this.get(`/user/${userID}/${lpad(year, 4)}/${lpad(month, 2)}/usage`, callback)
  }

  version (callback) {
    return this.get('/version', callback)
  }
}

module.exports = StatsClient
