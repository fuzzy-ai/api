// main.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const APIServer = require('./apiserver')

process.on('uncaughtException', (err) => {
  console.log('******************** UNCAUGHT EXCEPTION ********************')
  console.error(err)
  if (err.stack) {
    console.dir(err.stack.split('\n'))
  }
  return process.exit(127)
})

process.setMaxListeners(0)

const server = new APIServer(process.env)

server.start((err) => {
  if (err) {
    return console.error(err)
  } else {
    return console.log('Server listening')
  }
})

const shutdown = function () {
  console.log('Shutting down...')
  return server.stop((err) => {
    if (err) {
      console.error(err)
      return process.exit(-1)
    } else {
      console.log('Done.')
      return process.exit(0)
    }
  })
}

process.on('SIGTERM', shutdown)
process.on('SIGINT', shutdown)
