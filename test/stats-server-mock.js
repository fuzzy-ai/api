// stats-server-mock.js
// Copyright 2016 fuzzy.ai <evan@fuzzy.ai>
// All rights reserved.

const http = require('http')
const events = require('events')
const debug = require('debug')('api:stats-server-mock')
const uuid = require('uuid')
const _ = require('lodash')

const JSON_TYPE = 'application/json; charset=utf-8'

class StatsServerMock extends events.EventEmitter {
  constructor (code) {
    super()
    const self = this
    self.evaluations = {}
    self.feedbacks = {}
    self.usage = {}
    const server = http.createServer((request, response) => {
      let body = ''
      const respond = function (code, body) {
        debug(`${request.method} ${request.url} -> ${code} ${http.STATUS_CODES[code]}`)
        response.statusCode = code
        if (!response.headersSent) {
          response.setHeader('Content-Type', JSON_TYPE)
        }
        return response.end(JSON.stringify(body))
      }
      request.on('data', chunk => { body += chunk })
      request.on('error', err => respond(500, {status: 'error', message: err.message}))
      return request.on('end', () => {
        let el, id, key
        const auth = request.headers.authorization
        debug(`${request.method} ${request.url}`)
        if ((request.method === 'GET') && (request.url === '/version')) {
          return respond(200, {name: 'stats', version: '0.0.0'})
        } else if ((auth == null) || !auth.match(/^\s*Bearer\s+(.*)\s*$/) || (auth.match(/^\s*Bearer\s+(.*)\s*$/)[1] !== code)) {
          return respond(403, {status: 'error', message: 'No access'})
        } else if ((request.method === 'POST') && (request.url === '/evaluation')) {
          el = _.extend({createdAt: (new Date()).toISOString()}, JSON.parse(body))
          const now = new Date()
          const ym = now.toISOString().substr(0, 7)
          key = `${el.userID}-${ym}`
          if (!self.usage[key]) {
            self.usage[key] = 1
          } else {
            self.usage[key]++
          }
          self.evaluations[el.reqID] = el
          self.emit('evaluation', el)
          return respond(200, el)
        } else if ((request.method === 'POST') && request.url.match('^/evaluation/(.*)/feedback')) {
          [, id] = Array.from(request.url.match('^/evaluation/(.*)/feedback'))
          el = self.evaluations[id]
          if (!(self.feedbacks != null ? self.feedbacks[id] : undefined)) {
            self.feedbacks[id] = []
          }
          const props = {
            id: uuid.v1(),
            evaluationLog: id,
            data: JSON.parse(body),
            createdAt: (new Date()).toISOString()
          }
          self.feedbacks[id].push(props)
          return respond(200, props)
        } else if ((request.method === 'GET') && request.url.match('^/evaluation/(.*)/feedback')) {
          [, id] = Array.from(request.url.match('^/evaluation/(.*)/feedback'))
          el = self.evaluations[id]
          if ((el == null)) {
            return respond(404, {status: 'error', message: 'No feedback for that id'})
          } else {
            return respond(200, self.feedbacks[id])
          }
        } else if ((request.method === 'GET') && request.url.match('^/evaluation/(.*)$')) {
          [, id] = Array.from(request.url.match('^/evaluation/(.*)$'))
          el = self.evaluations[id]
          if ((el == null)) {
            return respond(404, {status: 'error', message: 'No feedback for that id'})
          } else {
            return respond(200, el)
          }
        } else if ((request.method === 'GET') && request.url.match('^/user/(.*)/(.*)/(.*)/usage$')) {
          let mon, year;
          [, id, year, mon] = Array.from(request.url.match('^/user/(.*)/(.*)/(.*)/usage$')) // eslint-disable-line prefer-const
          el = self.evaluations[id]
          key = `${id}-${year}-${mon}`
          if (self.usage[key]) {
            return respond(200, self.usage[key])
          } else {
            return respond(200, 0)
          }
        } else {
          // If we get here, no route found
          return respond(404, {status: 'error', message: `Cannot ${request.method} ${request.url}`})
        }
      })
    })

    this.start = function (callback) {
      server.once('error', err => callback(err))
      server.once('listening', () => callback(null))
      return server.listen(1516)
    }

    this.stop = function (callback) {
      server.once('close', () => callback(null))
      server.once('error', err => callback(err))
      return server.close()
    }
  }
}

module.exports = StatsServerMock
