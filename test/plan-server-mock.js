// plan-server-mock.js
// Copyright 2016 fuzzy.ai <evan@fuzzy.ai>
// All rights reserved.

const http = require('http')
const events = require('events')

const plans = require('./plans')

const JSON_TYPE = 'application/json; charset=utf-8'

class PlanServerMock extends events.EventEmitter {
  constructor (key) {
    super()

    const authorized = function (auth) {
      if ((auth == null)) {
        return false
      }

      const m = auth.match(/^\s*Bearer\s+(.*)\s*$/)

      if (!m) {
        return false
      }

      if (m[1] !== key) {
        return false
      }

      return true
    }

    const server = http.createServer((request, response) => {
      let body = '' // eslint-disable-line no-unused-vars
      const respond = function (code, body) {
        response.statusCode = code
        if (!response.headersSent) {
          response.setHeader('Content-Type', JSON_TYPE)
        }
        return response.end(JSON.stringify(body))
      }
      request.on('data', chunk => { body += chunk })
      request.on('error', err => respond(500, {status: 'error', message: err.message}))
      return request.on('end', () => {
        const auth = request.headers.authorization
        if ((request.method === 'GET') && (request.url === '/version')) {
          return respond(200, {name: 'plan', version: '0.0.0'})
        } else if (!authorized(auth)) {
          return respond(403, {status: 'error', message: 'No access'})
        } else if ((request.method === 'GET') && (request.url.substr(0, 6) === '/plan/')) {
          const id = request.url.substr(6)
          if (plans[id]) {
            return respond(200, plans[id])
          } else {
            return respond(404, {status: 'error', message: `No such plan ${id}`})
          }
        } else {
          // If we get here, no route found
          const verb = request.method
          const { url } = request
          return respond(404, {status: 'error', message: `Cannot ${verb} ${url}`})
        }
      })
    })

    this.start = function (callback) {
      server.once('error', err => callback(err))
      server.once('listening', () => callback(null))
      return server.listen(4815)
    }

    this.stop = function (callback) {
      server.once('close', () => callback(null))
      server.once('error', err => callback(err))
      return server.close()
    }
  }
}

module.exports = PlanServerMock
