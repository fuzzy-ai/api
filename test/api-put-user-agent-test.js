// api-put-user-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const async = require('async')
const request = require('request')
const _ = require('lodash')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

const createAgent = function (callback) {
  const options = {
    url: 'http://localhost:2342/agent',
    json: {
      inputs: {
        temperature: {
          cold: [50, 75],
          normal: [50, 75, 85, 100],
          hot: [85, 100]
        }
      },
      outputs: {
        fanSpeed: {
          slow: [50, 100],
          normal: [50, 100, 150, 200],
          fast: [150, 200]
        }
      },
      rules: [
        'IF temperature IS cold THEN fanSpeed IS slow',
        'IF temperature IS normal THEN fanSpeed IS normal',
        'IF temperature IS hot THEN fanSpeed IS fast'
      ]
    },
    headers: {
      authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
    }
  }
  request.post(options, (err, response, body) => {
    if (err) {
      return callback(err)
    } else if (response.statusCode !== 200) {
      return callback(new Error(`Bad status code ${response.statusCode}`))
    } else {
      return callback(null, body)
    }
  })
  return undefined
}

const updateAgent = function (agent, props, expected, callback) {
  if (!_.isFunction(callback)) {
    callback = expected
    expected = 200
  }
  const options = {
    url: `http://localhost:2342/agent/${agent.id}`,
    json: props,
    headers: {
      authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
    }
  }
  return request.put(options, (err, response, body) => {
    if (err) {
      return callback(err)
    } else if (response.statusCode !== expected) {
      return callback(new Error(`Bad status code ${response.statusCode}: ${JSON.stringify(body)}`))
    } else {
      return callback(null, body)
    }
  })
}

vows
  .describe('PUT /agent/:agentID')
  .addBatch(apiBatch({
    'and we update an agent with empty body': {
      topic () {
        async.waterfall([
          callback => createAgent(callback),
          (agent, callback) =>
            // true = empty body
            updateAgent(agent, true, 400, callback)

        ], this.callback)
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
        assert.isObject(data)
        assert.isString(data.status)
        assert.equal(data.status, 'error')
        assert.isString(data.message)
      }
    },
    'and we update an agent with bad data': {
      topic () {
        async.waterfall([
          callback => createAgent(callback),
          function (agent, callback) {
            const props = {
              inputs: {
                temperature: {
                  cold: [32, 75],
                  normal: [32, 75, 85, 100],
                  hot: [85, 100]
                }
              },
              outputs: {
                fanSpeed: {
                  slow: [50, 100],
                  normal: [50, 100, 150, 200],
                  fast: [150, 200]
                }
              },
              rules: 'IF temperature IS cold THEN fanSpeed IS slow'
            }
            return updateAgent(agent, props, 400, callback)
          }
        ], this.callback)
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
        assert.isObject(data)
        assert.isString(data.status)
        assert.equal(data.status, 'error')
        assert.isString(data.message)
      }
    },
    'and we create a new agent': {
      topic () {
        createAgent(this.callback)
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'and we update it': {
        topic (agent) {
          const props = {
            inputs: {
              temperature: {
                cold: [32, 75],
                normal: [32, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: [
              'IF temperature IS cold THEN fanSpeed IS slow',
              'IF temperature IS normal THEN fanSpeed IS normal',
              'IF temperature IS hot THEN fanSpeed IS fast'
            ]
          }
          updateAgent(agent, props, this.callback)
          return undefined
        },
        'it works' (err, data) {
          assert.ifError(err)
        },
        'data looks correct' (err, data) {
          assert.ifError(err)
          assert.isObject(data)
          assert.isObject(data.inputs)
          assert.isObject(data.outputs)
          assert.isArray(data.rules)
          assert.isString(data.id)
          assert.isString(data.userID)
        },
        'and we get the agent endpoint': {
          topic (afterPut, agent) {
            const { callback } = this
            const options = {
              url: `http://localhost:2342/agent/${agent.id}`,
              headers: {
                authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
              }
            }
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 200) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null, JSON.parse(body))
              }
            })
            return undefined
          },
          'it works' (err, data) {
            assert.ifError(err)
          },
          'data looks updated' (err, data) {
            assert.ifError(err)
            assert.isObject(data)
            assert.equal(__guard__(__guard__(data.inputs != null ? data.inputs.temperature : undefined, x1 => x1.cold), x => x[0]), 32)
          }
        }
      }
    },
    'and we create another new agent': {
      topic () {
        createAgent(this.callback)
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'and we update it with parsed_rules': {
        topic (agent) {
          const props = {
            inputs: {
              temperature: {
                cold: [32, 75],
                normal: [32, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            parsed_rules: [
              {type: 'increases', input: 'temperature', output: 'fanSpeed'}
            ]
          }
          updateAgent(agent, props, this.callback)
          return undefined
        },
        'it works' (err, data) {
          assert.ifError(err)
        },
        'data looks correct' (err, data) {
          assert.ifError(err)
          assert.isObject(data)
          assert.isObject(data.inputs)
          assert.isObject(data.outputs)
          assert.isArray(data.rules)
          assert.isString(data.id)
          assert.isString(data.userID)
        },
        'and we get the agent endpoint': {
          topic (afterPut, agent) {
            const { callback } = this
            const options = {
              url: `http://localhost:2342/agent/${agent.id}`,
              headers: {
                authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
              }
            }
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 200) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null, JSON.parse(body))
              }
            })
            return undefined
          },
          'it works' (err, data) {
            assert.ifError(err)
          },
          'data looks updated' (err, data) {
            assert.ifError(err)
            assert.isObject(data)
            assert.equal(data.rules.length, 1)
            assert.equal(data.parsed_rules.length, 1)
          }
        }
      }
    }
  })).export(module)

function __guard__ (value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined
}
