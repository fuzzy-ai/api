// evaluator-client-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const EvaluatorServerMock = require('./evaluator-server-mock')
const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('evaluator-client')
  .addBatch({
    'When we start a EvaluatorServerMock': {
      topic () {
        const { callback } = this
        const srv = new EvaluatorServerMock(env.EVALUATOR_KEY)
        srv.start((err) => {
          if (err) {
            return callback(err)
          } else {
            return callback(null, srv)
          }
        })
        return undefined
      },
      'it works' (err, srv) {
        assert.ifError(err)
        assert.isObject(srv)
      },
      teardown (srv) {
        srv.stop(this.callback)
      },
      'and we load the EvaluatorClient module': {
        topic () {
          const { callback } = this
          try {
            const EvaluatorClient = require('../lib/evaluator-client')
            callback(null, EvaluatorClient)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, EvaluatorClient) {
          assert.ifError(err)
          assert.isFunction(EvaluatorClient)
        },
        'and we create an EvaluatorClient': {
          topic (EvaluatorClient) {
            const { callback } = this
            try {
              const client = new EvaluatorClient(env.EVALUATOR_SERVER, env.EVALUATOR_KEY)
              callback(null, client)
            } catch (err) {
              callback(err)
            }
            return undefined
          },
          'it works' (err, client) {
            assert.ifError(err)
            assert.isObject(client)
          },
          'it has an evaluate() method' (err, client) {
            assert.ifError(err)
            assert.isFunction(client.evaluate)
          },
          'and we do an evaluation': {
            topic (client) {
              const { callback } = this
              const properties = {
                inputs: {
                  input1: {
                    low: [0, 1],
                    medium: [0, 1, 2],
                    high: [1, 2]
                  }
                },
                outputs: {
                  output1: {
                    low: [0, 1],
                    medium: [0, 1, 2],
                    high: [1, 2]
                  }
                },
                rules: ['input1 INCREASES output1']
              }
              const inputs =
                {input1: 1.3}
              client.evaluate(properties, inputs, (err, evaluation) => {
                if (err) {
                  return callback(err)
                } else {
                  return callback(null, evaluation)
                }
              })
              return undefined
            },
            'it works' (err, evaluation) {
              assert.ifError(err)
              assert.isObject(evaluation)
              assert.isObject(evaluation.fuzzified)
              assert.isArray(evaluation.rules)
              for (let i = 0; i < evaluation.rules.length; i++) {
                const rule = evaluation.rules[i]
                assert.isNumber(rule, `Rule ${i} is not a number: ${rule}`)
              }
              assert.isObject(evaluation.inferred)
              assert.isObject(evaluation.clipped)
              assert.isObject(evaluation.combined)
              assert.isObject(evaluation.centroid)
              assert.isObject(evaluation.crisp)
            }
          }
        }
      }
    }}).export(module)
