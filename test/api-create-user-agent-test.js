// api-create-user-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')
const _ = require('lodash')

const apiBatch = require('./apibatch')
const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('POST /agent')
  .addBatch(apiBatch({
    'and we create an agent with invalid sets': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            inputs: {
              temperature: {
                cold: 50,
                normal: 75,
                hot: 100
              }
            },
            outputs: {
              fanSpeed: {
                slow: 75,
                normal: 125,
                fast: 175
              }
            },
            rules: [
              'IF temperature IS cold THEN fanSpeed IS slow',
              'IF temperature IS normal THEN fanSpeed IS normal',
              'IF temperature IS hot THEN fanSpeed IS fast'
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode === 200) {
            return callback(new Error('Unexpected success'))
          } else if (response.statusCode !== 400) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it fails correctly' (err, body) {
        assert.ifError(err)
        assert.isObject(body)
        assert.isString(body.status)
        assert.equal(body.status, 'error')
        assert.isString(body.message)
      }
    },
    'and we create an agent with invalid rules': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: [
              'IF temperature IS cold THEN fanSpeed SHOULD INCREASE',
              'IF temperature IS normal THEN fanSpeed IS normal',
              'IF temperature IS hot THEN fanSpeed IS fast'
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode === 200) {
            return callback(new Error('Unexpected success'))
          } else if (response.statusCode !== 400) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it fails correctly' (err, body) {
        assert.ifError(err)
        assert.isObject(body)
        assert.isString(body.status)
        assert.equal(body.status, 'error')
        assert.isString(body.message)
      }
    },
    'and we create a new agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: [
              'IF temperature IS cold THEN fanSpeed IS slow',
              'IF temperature IS normal THEN fanSpeed IS normal',
              'IF temperature IS hot THEN fanSpeed IS fast'
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body, response.headers.location)
          }
        })
        return undefined
      },
      'it works' (err, data, location) {
        assert.ifError(err)
      },
      'location header looks right' (err, data, location) {
        assert.ifError(err)
        assert.isString(location)
        assert.match(location, /^http:\/\/localhost:2342\/agent\/\w+$/)
      },
      'data looks correct' (err, data, location) {
        assert.ifError(err)
        assert.isObject(data)
        assert.isString(data.name)
        assert.isObject(data.inputs)
        assert.isObject(data.outputs)
        assert.isArray(data.rules)
        assert.isString(data.id)
        assert.isString(data.userID)
      },
      'and we get the agent endpoint': {
        topic (agent, location, token) {
          const { callback } = this
          const options = {
            url: location,
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.get(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              return callback(new Error(`Bad status code ${response.statusCode}`))
            } else {
              return callback(null, JSON.parse(body))
            }
          })
          return undefined
        },
        'it works' (err, data) {
          assert.ifError(err)
        },
        'data looks correct' (err, data) {
          assert.ifError(err)
          assert.isObject(data)
          assert.isObject(data.inputs)
          assert.isObject(data.outputs)
          assert.isArray(data.rules)
          for (const rule of Array.from(data.rules)) {
            assert.isString(rule)
          }
          assert.isArray(data.parsed_rules)
          for (const parsedRule of Array.from(data.parsed_rules)) {
            assert.isObject(parsedRule)
          }
          assert.isString(data.id)
          assert.isString(data.userID)
        }
      },
      'and we get the list of the user agents': {
        topic (agent, location) {
          const { callback } = this
          const options = {
            url: 'http://localhost:2342/agent',
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.get(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              return callback(new Error(`Bad status code ${response.statusCode}`))
            } else {
              return callback(null, JSON.parse(body), agent)
            }
          })
          return undefined
        },
        'it works' (err, data, agent) {
          assert.ifError(err)
        },
        'data is an array of names and IDs' (err, data, agent) {
          assert.ifError(err)
          assert.isArray(data)
          return _.each(data, (datum) => {
            assert.isObject(datum)
            assert.isString(datum.id)
          })
        },
        'agent ID is in array' (err, data, agent) {
          assert.ifError(err)
          assert.isArray(data)
          const ids = _.map(data, 'id')
          assert.isTrue(_.some(ids, id => id === agent.id))
        }
      },
      'and we get the list of agent versions': {
        topic (agent, location) {
          const { callback } = this
          const options = {
            url: `http://localhost:2342/agent/${agent.id}/version`,
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.get(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              return callback(new Error(`Bad status code ${response.statusCode}`))
            } else {
              return callback(null, JSON.parse(body), agent)
            }
          })
          return undefined
        },
        'it works' (err, data, agent) {
          assert.ifError(err)
        },
        'data is an array of IDs' (err, data, agent) {
          assert.ifError(err)
          assert.isArray(data)
          return _.each(data, datum => assert.isString(datum))
        },
        'agent latestVersion is in array' (err, data, agent) {
          assert.ifError(err)
          assert.isArray(data)
          assert.isTrue(_.some(data, datum => datum === agent.latestVersion))
        }
      },
      'and we get the latest agent version': {
        topic (agent, location) {
          const { callback } = this
          const options = {
            url: `http://localhost:2342/version/${agent.latestVersion}`,
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.get(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              return callback(new Error(`Bad status code ${response.statusCode}: ${body}`))
            } else {
              return callback(null, response.headers, JSON.parse(body))
            }
          })
          return undefined
        },
        'it works' (err, headers, data) {
          assert.ifError(err)
        },
        'data looks correct' (err, headers, data) {
          assert.ifError(err)
          assert.isObject(data)
          assert.isObject(data.inputs)
          assert.isObject(data.outputs)
          for (const rule of Array.from(data.rules)) {
            assert.isString(rule)
          }
          assert.isArray(data.parsed_rules)
          for (const parsedRule of Array.from(data.parsed_rules)) {
            assert.isObject(parsedRule)
          }
          assert.isString(data.id)
          assert.isString(data.userID)
          assert.isString(data.versionOf)
          assert.isString(headers['last-modified'])
          const lm = Date.parse(headers['last-modified'])
          const cd = Date.parse(data.createdAt)
          assert.inDelta(lm, cd, 1000)
          assert.isString(headers['expires'])
          const ex = Date.parse(headers['expires'])
          assert.greater(ex, Date.now())
        }
      }
    },
    'and we create a new agent with just parsed_rules': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            parsed_rules: [
              {type: 'increases', input: 'temperature', output: 'fanSpeed'}
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body, response.headers.location)
          }
        })
        return undefined
      },
      'it works' (err, data, location) {
        assert.ifError(err)
      },
      'location header looks right' (err, data, location) {
        assert.ifError(err)
        assert.isString(location)
        assert.match(location, /^http:\/\/localhost:2342\/agent\/\w+$/)
      },
      'data looks correct' (err, data, location) {
        assert.ifError(err)
        assert.isObject(data)
        assert.isString(data.name)
        assert.isObject(data.inputs)
        assert.isObject(data.outputs)
        assert.isArray(data.rules)
        assert.isString(data.id)
        assert.isString(data.userID)
      }
    },
    'and we create a new agent with rules and parsed_rules': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: [
              'temperature INCREASES fanSpeed'
            ],
            parsed_rules: [
              {type: 'increases', input: 'temperature', output: 'fanSpeed'}
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body, response.headers.location)
          }
        })
        return undefined
      },
      'it works' (err, data, location) {
        assert.ifError(err)
      },
      'location header looks right' (err, data, location) {
        assert.ifError(err)
        assert.isString(location)
        assert.match(location, /^http:\/\/localhost:2342\/agent\/\w+$/)
      },
      'data looks correct' (err, data, location) {
        assert.ifError(err)
        assert.isObject(data)
        assert.isString(data.name)
        assert.isObject(data.inputs)
        assert.isObject(data.outputs)
        assert.isArray(data.rules)
        assert.isString(data.id)
        assert.isString(data.userID)
      }
    }
  })).export(module)
