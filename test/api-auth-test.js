// Copyright 2016 Fuzzy.ai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const env = require('./env')
const lr = require('./lr')
const apiBatch = require('./apibatch')

lr()

const BAD_KEY = 'sceneaqethyl1776podia'

const getFail = (options, callback) =>
  request.get(options, (err, response, body) => {
    const wa = __guard__(response != null ? response.headers : undefined, x => x['www-authenticate'])
    if (err) {
      return callback(err)
    } else if (response.statusCode !== 401) {
      return callback(new Error(`Unexpected code ${response.statusCode}`))
    } else if (wa !== 'Bearer') {
      return callback(new Error(`Wrong WWW-Authenticate header: ${wa}`))
    } else {
      return callback(null)
    }
  })

const getSucc = (options, callback) =>
  request.get(options, (err, response, body) => {
    if (err) {
      return callback(err)
    } else if (response.statusCode !== 200) {
      return callback(new Error(`Unexpected code ${response.statusCode}`))
    } else {
      return callback(null)
    }
  })

const authfail = function (key) {
  return {
    topic () {
      const options = {
        url: 'http://localhost:2342/agent',
        headers: {
          authorization: `Bearer ${key}`
        }
      }
      getFail(options, this.callback)
      return undefined
    },
    'it fails correctly' (err) {
      assert.ifError(err)
    }
  }
}

const authsucc = function (key) {
  return {
    topic () {
      const options = {
        url: 'http://localhost:2342/agent',
        headers: {
          authorization: `Bearer ${key}`
        }
      }
      getSucc(options, this.callback)
      return undefined
    },
    'it works' (err, results) {
      assert.ifError(err)
    }
  }
}

const qsucc = function (key) {
  return {
    topic () {
      const options =
        {url: `http://localhost:2342/agent?access_token=${key}`}
      getSucc(options, this.callback)
      return undefined
    },
    'it works' (err) {
      assert.ifError(err)
    }
  }
}

const qfail = function (key) {
  return {
    topic () {
      const options =
        {url: `http://localhost:2342/agent?access_token=${key}`}
      getFail(options, this.callback)
      return undefined
    },
    'it fails correctly' (err) {
      assert.ifError(err)
    }
  }
}

vows
  .describe('Authentication')
  .addBatch(apiBatch({
    'and we request the list of agents without any auth': {
      topic () {
        const options =
          {url: 'http://localhost:2342/agent'}
        getFail(options, this.callback)
        return undefined
      },
      'it fails correctly' (err) {
        assert.ifError(err)
      }
    },
    'and we request the list of agents with the Authorization header':
      authsucc(env.ENSURE_ACCESS_TOKEN),
    'and we request the list of agents with bad Authorization header':
      authfail(BAD_KEY),
    'and we request the list of agents with a good access_token parameter':
      qsucc(env.ENSURE_ACCESS_TOKEN),
    'and we request the list of agents with a bad access_token parameter':
      qfail(BAD_KEY)
  })).export(module)

function __guard__ (value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined
}
