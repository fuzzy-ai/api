// api-create-user-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const apiBatch = require('./apibatch')
const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('POST /agent with description')
  .addBatch(apiBatch({
    'and we create an agent with a description': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            description: 'As temperature goes up, so does the fan speed.',
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              'fan speed': {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: [
              'temperature increases [fan speed]'
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'data has a description' (err, data) {
        assert.ifError(err)
        assert.isObject(data)
        assert.isString(data.description)
        assert.equal(data.description, 'As temperature goes up, so does the fan speed.')
      },
      'and we get the agent': {
        topic (agent, token) {
          const { callback } = this
          const options = {
            url: `http://localhost:2342/agent/${agent.id}`,
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.get(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              return callback(new Error(`Bad status code ${response.statusCode}`))
            } else {
              return callback(null, JSON.parse(body))
            }
          })
          return undefined
        },
        'it works' (err, data) {
          assert.ifError(err)
        },
        'data has a description' (err, data) {
          assert.ifError(err)
          assert.isObject(data)
          assert.isString(data.description)
          assert.equal(data.description, 'As temperature goes up, so does the fan speed.')
        },
        'and we update the description': {
          topic (agent) {
            const { callback } = this
            const options = {
              url: `http://localhost:2342/agent/${agent.id}`,
              json: {
                name: 'Thermometer',
                description: 'As temperature goes up, fan speed goes up.',
                inputs: {
                  temperature: {
                    cold: [50, 75],
                    normal: [50, 75, 85, 100],
                    hot: [85, 100]
                  }
                },
                outputs: {
                  'fan speed': {
                    slow: [50, 100],
                    normal: [50, 100, 150, 200],
                    fast: [150, 200]
                  }
                },
                rules: [
                  'temperature increases [fan speed]'
                ]
              },
              headers: {
                authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
              }
            }
            request.put(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 200) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null, body)
              }
            })
            return undefined
          },
          'it works' (err, data, agent) {
            assert.ifError(err)
          },
          'data has a description' (err, data) {
            assert.ifError(err)
            assert.isObject(data)
            assert.isString(data.description)
            assert.equal(data.description, 'As temperature goes up, fan speed goes up.')
          },
          'and we get the agent': {
            topic (agent) {
              const { callback } = this
              const options = {
                url: `http://localhost:2342/agent/${agent.id}`,
                headers: {
                  authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
                }
              }
              request.get(options, (err, response, body) => {
                if (err) {
                  return callback(err)
                } else if (response.statusCode !== 200) {
                  return callback(new Error(`Bad status code ${response.statusCode}`))
                } else {
                  return callback(null, JSON.parse(body))
                }
              })
              return undefined
            },
            'it works' (err, data) {
              assert.ifError(err)
            },
            'data has a description' (err, data) {
              assert.ifError(err)
              assert.isObject(data)
              assert.isString(data.description)
              assert.equal(data.description, 'As temperature goes up, fan speed goes up.')
            }
          }
        }
      }
    }
  })).export(module)
