// api-cors-headers-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

vows
  .describe('OPTIONS /version')
  .addBatch(apiBatch({
    'and we do OPTIONS on the /version endpoint': {
      topic () {
        const { callback } = this
        const options = {
          method: 'OPTIONS',
          url: 'http://localhost:2342/version',
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if ((response.statusCode !== 204) && (response.statusCode !== 200)) {
            return callback(new Error(`Error ${response.statusCode}: ${JSON.stringify(body)}`))
          } else {
            return callback(null, response, body)
          }
        })
        return undefined
      },
      'it works' (err, response, body) {
        assert.ifError(err)
      },
      'it has the CORS headers' (err, response, data) {
        assert.ifError(err)
        assert.isObject(response)
        assert.isObject(response.headers)
        assert.isString(response.headers['access-control-allow-origin'])
        assert.isString(response.headers['access-control-allow-methods'])
        assert.isString(response.headers['access-control-allow-headers'])
      }
    }}))
  .export(module)
