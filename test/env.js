// env.js
// Copyright 2014,2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

module.exports = {
  HOSTNAME: 'localhost',
  PORT: '2342',
  DRIVER: 'memory',
  PARAMS: '{}',
  LOG_FILE: '/dev/null',
  ENSURE_USER: 'TESTUSER:fakename@email.test',
  ENSURE_ACCESS_TOKEN: 'TESTACCESSTOKEN',
  ILLEGAL_AGENT_ID: 'BADID',
  SILENT: 'true',
  STATS_SERVER: 'http://localhost:1516',
  STATS_KEY: 'drive-police-creak-strip-brainy',
  PLAN_SERVER: 'http://localhost:4815',
  PLAN_KEY: 'yrtatcloakobeynop',
  EVALUATOR_SERVER: 'http://localhost:2316',
  EVALUATOR_KEY: 'sorbblewjoancarlmaid',
  LEARNING_SERVER: 'http://localhost:4215',
  LEARNING_KEY: 'agenda-gang-peppy-porte-sn'
}
