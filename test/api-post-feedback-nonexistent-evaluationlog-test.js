// api-post-user-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

const NONEXISTENT = '364a51f6-0b2f-11e7-b3d3-c8f73398600c'

vows
  .describe('POST /evaluation/:logID/feedback with non-existent evaluation')
  .addBatch(apiBatch({
    'and we post training feedback': {
      topic () {
        const { callback } = this
        const options = {
          url: `http://localhost:2342/evaluation/${NONEXISTENT}/feedback`,
          json: {
            airflow: 10,
            ambient: 20
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            const code = response.statusCode
            const { message } = body
            return callback(new Error(`Bad status code ${code}: ${message}`))
          } else {
            return callback(null, response, body)
          }
        })
        return undefined
      },
      'it works' (err, response, body) {
        assert.ifError(err)
      },
      'it looks correct' (err, response, body) {
        assert.ifError(err)
        assert.isObject(body)
        assert.isString(body.id, 'Body has no ID')
        assert.isString(body.evaluationLog, 'Body has no evaluationLog')
        assert.isObject(body.data, 'Body has no data')
        assert.equal(body.data.airflow, 10, 'Data has no airflow')
        assert.equal(body.data.ambient, 20, 'Data has no ambient')
        assert.isString(body.createdAt, 'Body has not createdAt')
      }
    }
  })).export(module)
