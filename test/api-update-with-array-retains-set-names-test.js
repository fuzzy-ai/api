// api-update-with-array-retains-set-names-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')
const _ = require('lodash')

const apiBatch = require('./apibatch')
const env = require('./env')
const lr = require('./lr')

lr()

const base = {
  name: 'Heart Attack Risk',
  inputs: {
    age: {
      child: [0, 25],
      'young adult': [15, 35],
      'working age': [20, 50],
      'middle age': [35, 65],
      'senior': [55, 75]
    }
  },
  outputs: {
    'heart attack risk': {
      negligible: [0, 25],
      acceptable: [0, 25, 50],
      moderate: [25, 50, 75],
      'of concern': [50, 75, 100],
      urgent: [75, 100]
    }
  },
  rules: [
    'age INCREASES [heart attack risk]'
  ]
}

vows
  .describe('Using array does not change set names')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: base,
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            const code = response.statusCode
            const message = body != null ? body.message : undefined
            return callback(new Error(`Bad status code ${code}: ${message}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'data looks correct' (err, data) {
        assert.ifError(err)
        assert.isObject(data)
      },
      'and we update an input with an array': {
        topic (agent) {
          const { callback } = this
          const updated = _.cloneDeep(base)
          updated.inputs.age = [0, 85]
          updated.outputs['heart attack risk'] = [0, 1000]
          const options = {
            url: `http://localhost:2342/agent/${agent.id}`,
            json: updated,
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.put(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              const message = body != null ? body.message : undefined
              return callback(new Error(`Bad status code on update : ${message}`))
            } else {
              return callback(null, body)
            }
          })
          return undefined
        },
        'it works' (err, updated) {
          assert.ifError(err)
          assert.isObject(updated)
        },
        'input set names are the same' (err, updated) {
          assert.ifError(err)
          assert.isObject(updated)
          assert.deepEqual(_.sortBy(_.keys(updated.inputs.age)), _.sortBy(_.keys(base.inputs.age)))
          assert.deepEqual(_.sortBy(_.keys(updated.outputs['heart attack risk'])), _.sortBy(_.keys(base.outputs['heart attack risk'])))
        }
      }
    }
  })).export(module)
