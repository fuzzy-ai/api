// api-no-such-route-message-test.js
// Copyright 2017 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

const str = 'mall-her-quint-xf-faze'

vows
  .describe('GET non-existent route')
  .addBatch(apiBatch({
    'and we get a non-existent route': {
      topic () {
        const { callback } = this
        const options = {
          url: `http://localhost:2342/${str}`,
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.get(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 404) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
        assert.isTrue(data.indexOf(str) === -1, `Data $${data} should not contain string ${str}`)
      }
    }
  })).export(module)
