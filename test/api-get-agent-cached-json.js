// api-get-agent-cached-json.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

vows
  .describe('GET agent with caching headers')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: [
              'IF temperature IS cold THEN fanSpeed IS slow',
              'IF temperature IS normal THEN fanSpeed IS normal',
              'IF temperature IS hot THEN fanSpeed IS fast'
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'and we get the /agent endpoint': {
        topic (agent) {
          const { callback } = this
          const options = {
            url: `http://localhost:2342/agent/${agent.id}`,
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.get(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              return callback(new Error(`Bad status code ${response.statusCode}`))
            } else {
              return callback(null, response)
            }
          })
          return undefined
        },
        'it works' (err, response) {
          assert.ifError(err)
          assert.isObject(response.headers)
          assert.isString(response.headers['last-modified'])
          assert.isString(response.headers['etag'])
        },
        'and we re-get with the If-Modified-Since header': {
          topic (prev, agent) {
            const { callback } = this
            const options = {
              url: `http://localhost:2342/agent/${agent.id}`,
              headers: {
                'if-modified-since': prev.headers['last-modified'],
                authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
              }
            }
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 304) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null)
              }
            })
            return undefined
          },
          'it works' (err) {
            assert.ifError(err)
          }
        },
        'and we re-get with the If-None-Match header': {
          topic (prev, agent) {
            const { callback } = this
            const options = {
              url: `http://localhost:2342/agent/${agent.id}`,
              headers: {
                'if-none-match': prev.headers['etag'],
                authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
              }
            }
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 304) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null)
              }
            })
            return undefined
          },
          'it works' (err) {
            assert.ifError(err)
          }
        },
        'and we re-get with the If-None-Match and If-Modified-Since headers': {
          topic (prev, agent) {
            const { callback } = this
            const options = {
              url: `http://localhost:2342/agent/${agent.id}`,
              headers: {
                'if-none-match': prev.headers['etag'],
                'if-modified-since': prev.headers['last-modified'],
                authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
              }
            }
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 304) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null)
              }
            })
            return undefined
          },
          'it works' (err) {
            assert.ifError(err)
          }
        }
      }
    }
  })).export(module)
