// plans.js
// Copyright 2016 fuzzy.ai <evan@fuzzy.ai>
// All rights reserved.

const plans = {
  free: {
    name: 'Free Plan',
    stripeId: 'free',
    apiLimit: 1000,
    period: 'month',
    price: 0,
    currency: 'USD'
  },
  unit_test_1: {
    name: 'Unit Test 1',
    stripeId: 'unit_test_1',
    apiLimit: 5000,
    period: 'month',
    price: 2500,
    currency: 'USD'
  }
}

module.exports = plans
