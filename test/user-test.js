// user-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank

const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('User')
  .addBatch({
    'When we load the module': {
      topic () {
        try {
          const User = require('../lib/user')
          this.callback(null, User)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, User) {
        assert.ifError(err)
        assert.isFunction(User)
      },
      'and we set up the database': {
        topic (User) {
          const params = JSON.parse(env.PARAMS)
          params.schema = {user: User.schema}
          const db = Databank.get(env.DRIVER, params)
          db.connect({}, err => {
            if (err) {
              return this.callback(err)
            } else {
              DatabankObject.bank = db
              return this.callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        },
        'and we create a new user': {
          topic (User) {
            User.create({email: 'fakename@mail.localhost'}, this.callback)
            return undefined
          },
          'it works' (err, user) {
            assert.ifError(err)
          },
          'and we examine it': {
            topic (user) {
              return user
            },
            'it has the passed-in email address' (err, user) {
              assert.ifError(err)
              assert.equal(user.email, 'fakename@mail.localhost')
            },
            'it has an ID' (err, user) {
              assert.ifError(err)
              assert.isString(user.id)
            },
            'it has a createdAt timestamp' (err, user) {
              assert.ifError(err)
              assert.isString(user.createdAt)
              assert.inDelta(Date.parse(user.createdAt), Date.now(), 5000)
            },
            'it has an updatedAt timestamp' (err, user) {
              assert.ifError(err)
              assert.isString(user.updatedAt)
              assert.inDelta(Date.parse(user.updatedAt), Date.now(), 5000)
            }
          },
          'and we wait a couple of seconds': {
            topic (user) {
              setTimeout(this.callback, 2000, null)
              return undefined
            },
            'it works' (err) {
              assert.ifError(err)
            },
            'and we update the user object': {
              topic (user) {
                user.update({email: 'fakename@othermail.localhost'}, this.callback)
                return undefined
              },
              'it works' (err, user) {
                assert.ifError(err)
                assert.isObject(user)
                assert.equal(user.email, 'fakename@othermail.localhost')
              },
              'its update timestamp is updated' (err, user) {
                assert.ifError(err)
                assert.notEqual(user.updatedAt, user.createdAt)
                assert.inDelta(Date.parse(user.updatedAt), Date.now(), 5000)
              }
            }
          }
        }
      }
    }}).export(module)
