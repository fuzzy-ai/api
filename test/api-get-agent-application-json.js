// api-get-agent-application-json.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

vows
  .describe('GET /agent with application/json content type')
  .addBatch(apiBatch({
    'and we get the /agent endpoint with application/json content type': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          headers: {
            'Content-Type': 'application/json',
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.get(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, data, location) {
        assert.ifError(err)
      }
    }
  })).export(module)
