// api-post-user-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')
const _ = require('lodash')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

const withMeta = function (flag, property) {
  return {
    topic (agent, location) {
      const { callback } = this
      const options = {
        url: `${location}?meta=${flag}`,
        json: {
          temperature: 60
        },
        headers: {
          authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
        }
      }
      request.post(options, (err, response, body) => {
        if (err) {
          return callback(err)
        } else if (response.statusCode !== 200) {
          const msg = `Error ${response.statusCode}: ${JSON.stringify(body)}`
          return callback(new Error(msg))
        } else {
          return callback(null, response, body)
        }
      })
      return undefined
    },
    'it works' (err, response, data) {
      assert.ifError(err)
      assert.isObject(data)
    },
    [`it has the ${property} property`] (err, response, data) {
      let i, rule
      assert.ifError(err)
      assert.isObject(data[property])
      assert.isObject(data[property].fuzzified)
      assert.isArray(data[property].rules)
      for (i = 0; i < data[property].rules.length; i++) {
        rule = data[property].rules[i]
        assert.isNumber(rule, `Rule ${i} is not a number: ${rule}`)
      }
      assert.isArray(data[property].rules_text)
      assert.equal(data[property].rules_text.length, data[property].rules.length)
      for (i = 0; i < data[property].rules_text.length; i++) {
        const text = data[property].rules_text[i]
        assert.isString(text, `Rule text ${i} is not a string: ${rule}`)
      }
      assert.isObject(data[property].inferred)
      assert.isObject(data[property].clipped)
      assert.isObject(data[property].combined)
      assert.isObject(data[property].centroid)
    },
    'and we get the evaluation log': {
      topic (response, data) {
        const { callback } = this
        const id = data[property].reqID
        const options = {
          url: `http://localhost:2342/evaluation/${id}`,
          json: true,
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.get(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, log) {
        assert.ifError(err)
      },
      [`it does not have '${property}' in its crisp array`] (err, log) {
        assert.ifError(err)
        assert.isObject(log)
        assert.isObject(log.crisp)
        assert.notInclude(log.crisp, property)
      }
    }
  }
}

const withMetaBatch = function (flag, property) {
  return {
    topic (agent, location) {
      const { callback } = this
      const payload = __range__(40, 110, true).map(i => ({temperature: i}))
      const options = {
        url: `${location}?meta=${flag}`,
        json: payload,
        headers: {
          authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
        }
      }
      request.post(options, (err, response, body) => {
        if (err) {
          return callback(err)
        } else if (response.statusCode !== 200) {
          const msg = `Error ${response.statusCode}: ${JSON.stringify(body)}`
          return callback(new Error(msg))
        } else {
          return callback(null, response, body)
        }
      })
      return undefined
    },
    'it works' (err, response, data) {
      assert.ifError(err)
      assert.isArray(data)
      return Array.from(data).map((datum) =>
        assert.isObject(datum))
    },
    [`Each result has the ${property} property`] (err, response, data) {
      assert.ifError(err)
      assert.isArray(data)
      return (() => {
        const result = []
        for (const datum of Array.from(data)) {
          let i, rule
          assert.isObject(datum)
          const meta = datum[property]
          assert.isObject(meta)
          assert.isObject(meta.fuzzified)
          assert.isArray(meta.rules)
          for (i = 0; i < meta.rules.length; i++) {
            rule = meta.rules[i]
            assert.isNumber(rule, `Rule ${i} is not a number: ${rule}`)
          }
          assert.equal(meta.rules_text.length, meta.rules.length)
          assert.isArray(meta.rules_text)
          for (i = 0; i < meta.rules_text.length; i++) {
            const text = meta.rules_text[i]
            assert.isString(text, `Rule text ${i} is not a string: ${rule}`)
          }
          assert.isObject(meta.inferred)
          assert.isObject(meta.clipped)
          assert.isObject(meta.combined)
          result.push(assert.isObject(meta.centroid))
        }
        return result
      })()
    },
    'All results have a unique reqID' (err, response, data) {
      assert.ifError(err)
      assert.isArray(data)
      const metas = _.map(data, property)
      const ids = _.map(metas, 'reqID')
      const uniqueIDs = _.uniq(ids)
      assert.equal(data.length, uniqueIDs.length)
    }
  }
}
vows
  .describe('POST /agent/:agentID with meta flag')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: [
              'IF temperature IS cold THEN fanSpeed IS slow',
              'IF temperature IS normal THEN fanSpeed IS normal',
              'IF temperature IS hot THEN fanSpeed IS fast'
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body, response.headers.location)
          }
        })
        return undefined
      },
      'it works' (err, data, location) {
        assert.ifError(err)
      },
      'and we post to the agent without the meta flag': {
        topic (agent, location) {
          const { callback } = this
          const options = {
            url: location,
            json: {
              temperature: 60
            },
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.post(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              const msg = `Error ${response.statusCode}: ${JSON.stringify(body)}`
              return callback(new Error(msg))
            } else {
              return callback(null, response, body)
            }
          })
          return undefined
        },
        'it works' (err, response, data) {
          assert.ifError(err)
          assert.isObject(data)
        },
        'it does not have a meta property' (err, response, data) {
          assert.ifError(err)
          assert.isUndefined(data.audit)
        }
      },
      'and we post a batch of data without the meta flag': {
        topic (agent, location) {
          const { callback } = this
          const payload = __range__(40, 110, true).map(i => ({temperature: i}))
          const options = {
            url: `${location}`,
            json: payload,
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.post(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              const msg = `Error ${response.statusCode}: ${JSON.stringify(body)}`
              return callback(new Error(msg))
            } else {
              return callback(null, response, body)
            }
          })
          return undefined
        },
        'it works' (err, response, data) {
          assert.ifError(err)
          assert.isArray(data)
          return Array.from(data).map((datum) =>
            assert.isObject(datum))
        },
        'No result has the meta property' (err, response, data) {
          assert.ifError(err)
          assert.isArray(data)
          return Array.from(data).map((datum) =>
            assert.isUndefined(datum.meta))
        }
      },
      'and we post to the agent with the meta flag set to true':
        withMeta('true', 'meta'),
      'and we post to the agent with the meta flag set to yes':
        withMeta('yes', 'meta'),
      'and we post to the agent with the meta flag set to 1':
        withMeta('1', 'meta'),
      'and we post to the agent with the meta flag set to on':
        withMeta('on', 'meta'),
      'and we post to the agent with the meta flag set to a name':
        withMeta('audit', 'audit'),
      'and we post a batch to the agent with the meta flag set to true':
        withMetaBatch('true', 'meta'),
      'and we post a batch to the agent with the meta flag set to yes':
        withMetaBatch('yes', 'meta'),
      'and we post a batch to the agent with the meta flag set to 1':
        withMetaBatch('1', 'meta'),
      'and we post a batch to the agent with the meta flag set to on':
        withMetaBatch('on', 'meta'),
      'and we post a batch to the agent with the meta flag set to a name':
        withMetaBatch('audit', 'audit')
    }
  })).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}
