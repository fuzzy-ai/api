// api-post-user-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

vows
  .describe('POST /agent/:agentID')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: [
              'IF temperature IS cold THEN fanSpeed IS slow',
              'IF temperature IS normal THEN fanSpeed IS normal',
              'IF temperature IS hot THEN fanSpeed IS fast'
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body, response.headers.location)
          }
        })
        return undefined
      },
      'it works' (err, data, location) {
        assert.ifError(err)
      },
      'and we post to the agent': {
        topic (agent, location, api, mocks) {
          const { callback } = this
          const options = {
            url: location,
            json: {
              temperature: 60
            },
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          let gotRequest = false
          mocks.evaluator.on('request', (req, res) => { gotRequest = true })
          request.post(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              const code = response.statusCode
              const msg = body != null ? body.message : undefined
              return callback(new Error(`Error ${code}: ${msg}`))
            } else {
              return callback(null, response, body, gotRequest)
            }
          })
          return undefined
        },
        'it works' (err, response, data, gotRequest) {
          assert.ifError(err)
          assert.isObject(data)
        },
        'it has the right information' (err, response, data, gotRequest) {
          assert.ifError(err)
          assert.isObject(data)
          assert.isNumber(data['fanSpeed'])
          assert.inDelta(data['fanSpeed'], 94, 2)
        },
        'it has an X-Evaluation-ID header' (err, response, data, gotRequest) {
          assert.ifError(err)
          assert.isObject(response)
          assert.isObject(response.headers)
          assert.isString(response.headers['x-evaluation-id'])
          const uuid = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/i
          assert.match(response.headers['x-evaluation-id'], uuid)
        },
        // 'it has the correct rate-limiting headers': (err, response, data, gotRequest) ->
        //   assert.ifError err
        //   headers = response.headers
        //   assert.isString headers['x-rate-limit-limit']
        //   limit = parseInt headers['x-rate-limit-limit'], 10
        //   assert.equal limit, plans.free.apiLimit
        //   assert.isString headers['x-rate-limit-remaining']
        //   remaining = parseInt headers['x-rate-limit-remaining'], 10
        //   assert.greater remaining, 0
        //   assert.isString headers['x-rate-limit-reset']
        //   reset = parseInt headers['x-rate-limit-reset'], 10
        //   # Reset should be greater than 0, less than a month
        //   assert.greater reset, 0
        //   assert.lesser reset, 31 * 24 * 60 * 60
        'the evaluator got a request' (err, response, data, gotRequest) {
          assert.ifError(err)
          assert.ok(gotRequest)
        },
        'and we get the evaluation log': {
          topic (response, data, gotRequest, agent, location) {
            const { callback } = this
            const id = response.headers['x-evaluation-id']
            const options = {
              url: `http://localhost:2342/evaluation/${id}`,
              json: true,
              headers: {
                authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
              }
            }
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 200) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null, response, body)
              }
            })
            return undefined
          },
          'it works' (err, response, el) {
            assert.ifError(err)
          },
          'it looks correct' (err, response, el) {
            let i, rule
            assert.ifError(err)
            assert.isObject(el)
            assert.isString(el.userID)
            assert.isString(el.agentID)
            assert.isString(el.versionID)
            assert.isObject(el.input)
            assert.isArray(el.rules)
            for (i = 0; i < el.rules.length; i++) {
              rule = el.rules[i]
              assert.isNumber(rule, `Rule ${i} is not a number: ${rule}`)
            }
            assert.isArray(el.rules_text)
            for (i = 0; i < el.rules_text.length; i++) {
              const text = el.rules_text[i]
              assert.isString(text, `Rule text ${i} is not a string: ${rule}`)
            }
            assert.isObject(el.inferred)
            assert.isObject(el.clipped)
            assert.isObject(el.combined)
            assert.isObject(el.centroid)
            assert.isObject(el.crisp)
            assert.isString(el.createdAt)
            assert.isString(response.headers['last-modified'])
            const lm = Date.parse(response.headers['last-modified'])
            const cd = Date.parse(el.createdAt)
            assert.inDelta(lm, cd, 1000)
            assert.isString(response.headers['expires'])
            const ex = Date.parse(response.headers['expires'])
            assert.greater(ex, Date.now())
          }
        },
        'and we post training feedback': {
          topic (response, data, gotRequest, agent, location) {
            const { callback } = this
            const id = response.headers['x-evaluation-id']
            const options = {
              url: `http://localhost:2342/evaluation/${id}/feedback`,
              json: {
                airflow: 10,
                ambient: 20
              },
              headers: {
                authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
              }
            }
            request.post(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 200) {
                const code = response.statusCode
                const { message } = body
                return callback(new Error(`Bad status code ${code}: ${message}`))
              } else {
                return callback(null, response, body)
              }
            })
            return undefined
          },
          'it works' (err, response, body) {
            assert.ifError(err)
          },
          'it looks correct' (err, response, body) {
            assert.ifError(err)
            assert.isObject(body)
            assert.isString(body.id)
            assert.isString(body.evaluationLog)
            assert.isObject(body.data)
            assert.equal(body.data.airflow, 10)
            assert.equal(body.data.ambient, 20)
            assert.isString(body.createdAt)
          },
          'and we get the training feedback': {
            topic (fbresponse, feedback, response, data, gotRequest, agent, location) {
              const { callback } = this
              const id = response.headers['x-evaluation-id']
              const options = {
                url: `http://localhost:2342/evaluation/${id}/feedback`,
                json: true,
                headers: {
                  authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
                }
              }
              request.get(options, (err, response, body) => {
                if (err) {
                  return callback(err)
                } else if (response.statusCode !== 200) {
                  return callback(new Error(`Bad status code ${response.statusCode}`))
                } else {
                  return callback(null, response, body)
                }
              })
              return undefined
            },
            'it works' (err, response, body) {
              assert.ifError(err)
            },
            'it looks correct' (err, response, body) {
              assert.ifError(err)
              assert.isArray(body)
              assert.lengthOf(body, 1)
              assert.isString(body[0].id)
              assert.isString(body[0].evaluationLog)
              assert.isObject(body[0].data)
              assert.equal(body[0].data.airflow, 10)
              assert.equal(body[0].data.ambient, 20)
              assert.isString(body[0].createdAt)
            }
          }
        }
      }
    }
  })).export(module)
