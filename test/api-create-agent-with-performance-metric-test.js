// api-create-user-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const async = require('async')
const request = require('request')
const _ = require('lodash')
const debug = require('debug')('api:api-create-agent-with-performance-metric')

const apiBatch = require('./apibatch')
const env = require('./env')
const lr = require('./lr')

lr()

const ballistics = {
  name: 'Ballistics',
  inputs: {
    'distance to target': {
      near: [0, 500],
      middle: [0, 500, 1000],
      far: [500, 1000]
    }
  },
  outputs: {
    angle: {
      low: [0, Math.PI / 8.0],
      normal: [0, Math.PI / 8.0, Math.PI / 4.0],
      fast: [Math.PI / 8.0, Math.PI / 4.0]
    }
  },
  rules: [
    '[distance to target] INCREASES angle'
  ],
  performance: {
    'missed by': 'minimize',
    'damage': 'maximize'
  }
}

const postTopic = function (url, payload, callback) {
  debug({fn: 'postTopic', url, payload})
  const options = {
    url,
    json: payload,
    headers: {
      authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
    }
  }
  request.post(options, (err, response, body) => {
    if (err) {
      return callback(err)
    } else if (response.statusCode !== 200) {
      const code = response.statusCode
      const { message } = body
      return callback(new Error(`Bad status code ${code}: ${message}`))
    } else {
      return callback(null, body)
    }
  })
  return undefined
}

const getTopic = function (url, callback) {
  debug({fn: 'getTopic', url})
  const options = {
    url,
    headers: {
      authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
    }
  }
  request.get(options, (err, response, body) => {
    if (err) {
      return callback(err)
    } else if (response.statusCode !== 200) {
      const code = response.statusCode
      const { message } = JSON.parse(body)
      return callback(new Error(`Bad status code ${code}: ${message}`))
    } else {
      return callback(null, JSON.parse(body))
    }
  })
  return undefined
}

const putTopic = function (url, payload, callback) {
  debug({fn: 'getTopic', url})
  const options = {
    url,
    json: payload,
    headers: {
      authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
    }
  }
  request.put(options, (err, response, body) => {
    if (err) {
      return callback(err)
    } else if (response.statusCode !== 200) {
      const code = response.statusCode
      const { message } = body
      return callback(new Error(`Bad status code ${code}: ${message}`))
    } else {
      return callback(null, body)
    }
  })
  return undefined
}

vows
  .describe('POST /agent with performance metrics')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        return postTopic('http://localhost:2342/agent', ballistics, this.callback)
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'performance properties look correct' (err, data) {
        assert.ifError(err)
        assert.isObject(data)
        assert.isObject(data.performance)
        assert.isString(data.performance['missed by'])
        assert.equal(data.performance['missed by'], 'minimize')
        assert.isString(data.performance['damage'])
        assert.equal(data.performance['damage'], 'maximize')
      },
      'and we get the agent endpoint': {
        topic (agent, location, token) {
          const url = `http://localhost:2342/agent/${agent.id}`
          debug(`Getting ${url}`)
          return getTopic(url, this.callback)
        },
        'it works' (err, data) {
          assert.ifError(err)
        },
        'performance properties look correct' (err, data) {
          assert.ifError(err)
          assert.isObject(data)
          assert.isObject(data.performance)
          assert.isString(data.performance['missed by'])
          assert.equal(data.performance['missed by'], 'minimize')
          assert.isString(data.performance['damage'])
          assert.equal(data.performance['damage'], 'maximize')
        },
        'and we add a performance metric': {
          topic (agent) {
            const url = `http://localhost:2342/agent/${agent.id}`
            const updated = _.cloneDeep(agent)
            updated.performance.casualties = 'maximize'
            return putTopic(url, updated, this.callback)
          },
          'it works' (err, data) {
            assert.ifError(err)
          },
          'performance properties look correct' (err, data) {
            assert.ifError(err)
            assert.isObject(data)
            assert.isObject(data.performance)
            assert.isString(data.performance['missed by'])
            assert.equal(data.performance['missed by'], 'minimize')
            assert.isString(data.performance['damage'])
            assert.equal(data.performance['damage'], 'maximize')
            assert.isString(data.performance['casualties'])
            assert.equal(data.performance['casualties'], 'maximize')
          },
          'and we remove a performance metric': {
            topic (agent) {
              const url = `http://localhost:2342/agent/${agent.id}`
              const updated = _.cloneDeep(agent)
              delete updated.performance.damage
              return putTopic(url, updated, this.callback)
            },
            'it works' (err, data) {
              assert.ifError(err)
            },
            'performance properties look correct' (err, data) {
              assert.ifError(err)
              assert.isObject(data)
              assert.isObject(data.performance)
              assert.isString(data.performance['missed by'])
              assert.equal(data.performance['missed by'], 'minimize')
              assert.isString(data.performance['casualties'])
              assert.equal(data.performance['casualties'], 'maximize')
              assert.isUndefined(data.performance.damage)
            },
            'and we provide feedback': {
              topic (agent) {
                async.waterfall([
                  function (callback) {
                    const url = `http://localhost:2342/agent/${agent.id}?meta=true`
                    return postTopic(url, {'distance to target': 600}, callback)
                  },
                  function (outputs, callback) {
                    debug(`Outputs = ${_.keys(outputs)}`)
                    debug(`Meta = ${JSON.stringify(outputs.meta)}`)
                    const id = outputs.meta.reqID
                    debug(`ID = ${id}`)
                    const url = `http://localhost:2342/evaluation/${id}/feedback`
                    return postTopic(url, {'muzzle temperature': 250}, callback)
                  },
                  function (feedback, callback) {
                    debug(feedback)
                    const url = `http://localhost:2342/agent/${agent.id}`
                    return getTopic(url, callback)
                  }
                ], this.callback)
                return undefined
              },
              'it works' (err, data) {
                assert.ifError(err)
              },
              'it looks correct' (err, data) {
                assert.ifError(err)
                assert.isObject(data)
                assert.isObject(data.performance)
                assert.isString(data.performance['missed by'])
                assert.equal(data.performance['missed by'], 'minimize')
                assert.isString(data.performance['casualties'])
                assert.equal(data.performance['casualties'], 'maximize')
                assert.isString(data.performance['muzzle temperature'])
                assert.equal(data.performance['muzzle temperature'], 'minimize')
              }
            }
          }
        }
      }
    }
  })).export(module)
