// api-create-user-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')
const debug = require('debug')('api:api-create-default-empty-agent')

const apiBatch = require('./apibatch')
const env = require('./env')
const lr = require('./lr')

lr()

const agent = {
  'inputs': {},
  'outputs': { 'relevance': [0, 100] },
  'rules': [],
  'name': 'relevance'
}

const postTopic = function (url, payload, callback) {
  debug({fn: 'postTopic', url, payload})
  const options = {
    url,
    json: payload,
    headers: {
      authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
    }
  }
  request.post(options, (err, response, body) => {
    if (err) {
      return callback(err)
    } else if (response.statusCode !== 200) {
      const code = response.statusCode
      const { message } = body
      return callback(new Error(`Bad status code ${code}: ${message}`))
    } else {
      return callback(null, body)
    }
  })
  return undefined
}

vows
  .describe('POST /agent with default empty agent')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        return postTopic('http://localhost:2342/agent', agent, this.callback)
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'properties look correct' (err, data) {
        assert.ifError(err)
        assert.isObject(data)
      }
    }
  })).export(module)
