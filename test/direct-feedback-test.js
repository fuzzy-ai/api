// api-direct-feedback-test.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const async = require('async')
const request = require('request')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

const def = {
  name: 'Thermometer',
  inputs: {
    temperature: {
      cold: [50, 75],
      normal: [50, 75, 85, 100],
      hot: [85, 100]
    }
  },
  outputs: {
    fanSpeed: {
      slow: [50, 100],
      normal: [50, 100, 150, 200],
      fast: [150, 200]
    }
  },
  rules: [
    'IF temperature IS cold THEN fanSpeed IS slow',
    'IF temperature IS normal THEN fanSpeed IS normal',
    'IF temperature IS hot THEN fanSpeed IS fast'
  ]
}

vows
  .describe('Direct feedback learning test')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: def,
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, body) {
        assert.ifError(err)
        assert.isObject(body)
      },
      'and we post to the agent with meta': {
        topic (agent) {
          const { callback } = this
          const options = {
            url: `http://localhost:2342/agent/${agent.id}?meta=true`,
            json: {
              temperature: 60
            },
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.post(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              const code = response.statusCode
              const msg = body != null ? body.message : undefined
              return callback(new Error(`Error ${code}: ${msg}`))
            } else {
              return callback(null, body != null ? body.meta : undefined)
            }
          })
          return undefined
        },
        'it works' (err, evaluation) {
          assert.ifError(err)
          assert.isObject(evaluation)
          assert.isString(evaluation.reqID)
        },
        'and we post direct feedback': {
          topic (evaluation, agent, api, mocks) {
            const { callback } = this
            async.parallel([
              function (callback) {
                const neverCalled = () => callback(new Error('Learning not called after 10 seconds'))
                const timeout = setTimeout(neverCalled, 10000)
                return mocks.learning.on('request', () => {
                  clearTimeout(timeout)
                  return callback(null)
                })
              },
              function (callback) {
                const options = {
                  url: `http://localhost:2342/evaluation/${evaluation.reqID}/feedback`,
                  json: {
                    fanSpeed: 125
                  },
                  headers: {
                    authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
                  }
                }
                return request.post(options, (err, response, body) => {
                  if (err) {
                    return callback(err)
                  } else if (response.statusCode !== 200) {
                    const code = response.statusCode
                    const { message } = body
                    return callback(new Error(`Bad status code ${code}: ${message}`))
                  } else {
                    return callback(null)
                  }
                })
              }
            ], err => callback(err))
            return undefined
          },
          'it works' (err) {
            assert.ifError(err)
          }
        }
      }
    }
  })).export(module)
