// api-get-nonexistent-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

vows
  .describe('GET /agent/:agentID (non-existent)')
  .addBatch(apiBatch({
    'and we get a non-existent agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent/NONEXISTENT',
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.get(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 404) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, data, location) {
        assert.ifError(err)
      }
    }
  })).export(module)
