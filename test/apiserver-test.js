// apiserver-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('API server')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const APIServer = require('../lib/apiserver')
          callback(null, APIServer)
        } catch (err) {
          callback(err)
        }
        return undefined
      },
      'it works' (err, APIServer) {
        assert.ifError(err)
      },
      'it is a class' (err, APIServer) {
        assert.ifError(err)
        assert.isFunction(APIServer)
      },
      'and we instantiate an APIServer': {
        topic (APIServer) {
          const { callback } = this
          try {
            const server = new APIServer(env)
            callback(null, server)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          assert.ifError(err)
        },
        'it is an object' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
        },
        'it has a start() method' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
          assert.isFunction(server.start)
        },
        'it has a stop() method' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
          assert.isFunction(server.stop)
        },
        'and we start the server': {
          topic (server) {
            const { callback } = this
            try {
              server.start((err) => {
                if (err) {
                  return callback(err)
                } else {
                  return callback(null)
                }
              })
            } catch (error) {
              const err = error
              callback(err)
            }
            return undefined
          },
          'it works' (err) {
            assert.ifError(err)
          },
          'and we request the version': {
            topic () {
              const { callback } = this
              const url = 'http://localhost:2342/version'
              request.get(url, (err, response, body) => {
                if (err) {
                  return callback(err)
                } else if (response.statusCode !== 200) {
                  return callback(new Error(`Bad status code ${response.statusCode}`))
                } else {
                  return callback(null, JSON.parse(body))
                }
              })
              return undefined
            },
            'it works' (err, version) {
              assert.ifError(err)
              assert.isString(version.version)
              assert.isString(version.name)
            },
            'and we stop the server': {
              topic (version, server) {
                const { callback } = this
                server.stop((err) => {
                  if (err) {
                    return callback(err)
                  } else {
                    return callback(null)
                  }
                })
                return undefined
              },
              'it works' (err) {
                assert.ifError(err)
              },
              'and we request the version': {
                topic () {
                  const { callback } = this
                  const url = 'http://localhost:2342/version'
                  request.get(url, (err, response, body) => {
                    if (err) {
                      return callback(null)
                    } else {
                      return callback(new Error('Unexpected success after server stop'))
                    }
                  })
                  return undefined
                },
                'it fails correctly' (err) {
                  assert.ifError(err)
                }
              }
            }
          }
        }
      }
    }}).export(module)
