// api-agent-list-last-modified-test.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')
const _ = require('lodash')
const async = require('async')
const debug = require('debug')('api:api-agent-list-last-modified-test')

const apiBatch = require('./apibatch')
const env = require('./env')
const lr = require('./lr')

lr()

const thermometer = {
  inputs: {
    temperature: {
      cold: [50, 75],
      normal: [50, 75, 85, 100],
      hot: [85, 100]
    }
  },
  outputs: {
    fanSpeed: {
      slow: [50, 100],
      normal: [50, 100, 150, 200],
      fast: [150, 200]
    }
  },
  rules: [
    'IF temperature IS cold THEN fanSpeed IS slow',
    'IF temperature IS normal THEN fanSpeed IS normal',
    'IF temperature IS hot THEN fanSpeed IS fast'
  ]
}

const AGENT_COUNT = 3

const getAgentListLM = function (callback) {
  const options = {
    url: 'http://localhost:2342/agent',
    headers: {
      authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
    }
  }

  debug(`Getting agent list at ${options.url}`)

  return request.get(options, (err, response, body) => {
    if (err) {
      debug(`Error getting agent list: ${err}`)
      return callback(err)
    } else if (response.statusCode !== 200) {
      const code = response.statusCode
      const msg = JSON.parse(body).message
      debug(`Unexpected code ${code}: ${msg}`)
      return callback(new Error(`Bad status code ${code}: ${msg}`))
    } else {
      debug('Successfully retrieved agent list')
      const lm = response.headers['last-modified']
      debug(`Last-Modified = ${lm}`)
      return callback(null, lm)
    }
  })
}

vows
  .describe('GET /agent has right Last-Modified')
  .addBatch(apiBatch({
    'and we get the /agent endpoint': {

      topic () {
        getAgentListLM(this.callback)
        return undefined
      },

      'it has a Last-Modified header' (err, lm) {
        assert.ifError(err)
        assert.isString(lm)
      },

      'and we create several agents': {
        topic () {
          const newAgent = function (i, callback) {
            const createAgent = function () {
              const therm = _.clone(thermometer)
              therm.name = `Thermometer ${i}`
              const options = {
                url: 'http://localhost:2342/agent',
                json: therm,
                headers: {
                  authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
                }
              }

              return request.post(options, (err, response, body) => {
                if (err) {
                  return callback(err)
                } else if (response.statusCode !== 200) {
                  const code = response.statusCode
                  const msg = body != null ? body.message : undefined
                  return callback(new Error(`Bad status code ${code}: ${msg}`))
                } else {
                  return callback(null, body.id)
                }
              })
            }

            // We space them out by 2s so that their timestamps will
            // be distinct

            return setTimeout(createAgent, 2000)
          }

          async.timesSeries(AGENT_COUNT, newAgent, (err, ids) => {
            if (err) {
              return this.callback(err)
            } else {
              // Callback with last create time, roughly
              // Use the same format as Last-Modified so we don't go insane
              return this.callback(null, ids, (new Date()).toUTCString())
            }
          })

          return undefined
        },

        'it works' (err, ids, lc) {
          assert.ifError(err)
          assert.isString(lc)
        },

        'and we get the /agent endpoint': {

          topic (ids, lc, lm) {
            getAgentListLM((err, newLM) => {
              if (err) {
                return this.callback(err)
              } else {
                return this.callback(null, newLM, lc, lm)
              }
            })

            return undefined
          },

          'it works' (err, newLM, lc, lm) {
            assert.ifError(err)
            assert.isString(newLM)
            assert.greater(Date.parse(newLM), Date.parse(lm),
              'Last-Modified after create not greater than before create')
            assert.inDelta(Date.parse(newLM), Date.parse(lc), 1000,
              'Last-Modified after create not about the time of last create')
          },

          'and we delete an agent': {

            topic (lm, lc, oldLM, ids) {
              const deleteAgent = () => {
                const options = {
                  url: `http://localhost:2342/agent/${ids[0]}`,
                  headers: {
                    authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
                  }
                }

                return request.del(options, (err, response, body) => {
                  if (err) {
                    return this.callback(err)
                  } else if (response.statusCode !== 200) {
                    const code = response.statusCode
                    const msg = body != null ? body.message : undefined
                    return this.callback(new Error(`Bad status code ${code}: ${msg}`))
                  } else {
                    return this.callback(null, (new Date()).toUTCString())
                  }
                })
              }

              debug(ids)

              setTimeout(deleteAgent, 2000)

              return undefined
            },

            'it works' (err, ld) {
              assert.ifError(err)
              assert.isString(ld)
            },

            'and we get the /agent endpoint again': {
              topic (ld, oldLM) {
                getAgentListLM((err, lm) => {
                  if (err) {
                    return this.callback(err)
                  } else {
                    return this.callback(null, lm, ld, oldLM)
                  }
                })

                return undefined
              },

              'it works' (err, lm, ld, oldLM) {
                assert.ifError(err)
                assert.isString(lm)
                assert.isNumber(Date.parse(lm))
                assert.isNumber(Date.parse(oldLM))
                assert.greater(Date.parse(lm), Date.parse(oldLM))
                assert.inDelta(Date.parse(lm), Date.parse(ld), 1000)
              },

              'and we create one more agent': {

                topic () {
                  const createAgent = () => {
                    const therm = _.clone(thermometer)
                    therm.name = `Thermometer ${AGENT_COUNT + 1}`
                    const options = {
                      url: 'http://localhost:2342/agent',
                      json: therm,
                      headers: {
                        authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
                      }
                    }

                    return request.post(options, (err, response, body) => {
                      if (err) {
                        return this.callback(err)
                      } else if (response.statusCode !== 200) {
                        const code = response.statusCode
                        const msg = body != null ? body.message : undefined
                        return this.callback(new Error(`Bad status code ${code}: ${msg}`))
                      } else {
                        return this.callback(null, (new Date()).toUTCString())
                      }
                    })
                  }

                  setTimeout(createAgent, 2000)

                  return undefined
                },

                'it works' (err, llc) {
                  assert.ifError(err)
                  assert.isString(llc)
                },

                'and we get the /agent endpoint one last time': {

                  topic (llc, oldLM) {
                    getAgentListLM((err, lm) => {
                      if (err) {
                        return this.callback(err)
                      } else {
                        return this.callback(null, lm, llc, oldLM)
                      }
                    })

                    return undefined
                  },

                  'it works' (err, lm, llc, oldLM) {
                    assert.ifError(err)
                    assert.isString(lm)
                    assert.isNumber(Date.parse(lm))
                    assert.isNumber(Date.parse(oldLM))
                    assert.greater(Date.parse(lm), Date.parse(oldLM))
                    assert.inDelta(Date.parse(lm), Date.parse(llc), 1000)
                  }
                }
              }
            }
          }
        }
      }
    }
  })).export(module)
