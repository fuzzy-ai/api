// stats-server-mock.js
// Copyright 2016 fuzzy.ai <evan@fuzzy.ai>
// All rights reserved.

const http = require('http')
const events = require('events')

const JSON_TYPE = 'application/json; charset=utf-8'

class LearningServerMock extends events.EventEmitter {
  constructor (code) {
    super()
    const self = this
    const server = http.createServer((request, response) => {
      let body = ''
      const respond = function (code, body) {
        response.statusCode = code
        if (!response.headersSent) {
          response.setHeader('Content-Type', JSON_TYPE)
        }
        return response.end(JSON.stringify(body))
      }
      request.on('data', chunk => { body += chunk })
      request.on('error', err => respond(500, {status: 'error', message: err.message}))
      return request.on('end', () => {
        const auth = request.headers.authorization
        const bearer = /^\s*Bearer\s+(.*)\s*$/
        if ((request.method === 'GET') && (request.url === '/version')) {
          return respond(200, {name: 'learning', version: '0.0.0'})
        } else if ((auth == null) || !auth.match(bearer) || (auth.match(bearer)[1] !== code)) {
          return respond(403, {status: 'error', message: 'No access'})
        } else if ((request.method === 'GET') && (request.url === '/version')) {
          return respond(200, {name: 'learning', version: '0.2.0-mock'})
        } else if ((request.method === 'POST') && (request.url === '/optimize')) {
          const {agent} = JSON.parse(body)
          // XXX: change the agent somehow
          return respond(200, agent)
        } else {
          // If we get here, no route found
          return respond(404, {
            status: 'error',
            message: `Cannot ${request.method} ${request.url}`
          }
          )
        }
      })
    })

    server.on('request', (req, res) => self.emit('request', req, res))

    this.start = function (callback) {
      server.once('error', err => callback(err))
      server.once('listening', () => callback(null))
      return server.listen(4215)
    }

    this.stop = function (callback) {
      server.once('close', () => callback(null))
      server.once('error', err => callback(err))
      return server.close()
    }
  }
}

module.exports = LearningServerMock
