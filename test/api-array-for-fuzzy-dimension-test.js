// api-array-for-fuzzy-dimension-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')
const _ = require('lodash')

const apiBatch = require('./apibatch')
const env = require('./env')
const lr = require('./lr')

lr()

const base = {
  name: 'Thermometer',
  inputs: {
    temperature: {
      cold: [50, 75],
      normal: [50, 75, 85, 100],
      hot: [85, 100]
    }
  },
  outputs: {
    fanSpeed: {
      slow: [50, 100],
      normal: [50, 100, 150, 200],
      fast: [150, 200]
    }
  },
  rules: [
    'temperature INCREASES fanSpeed'
  ]
}

const creationBatch = function (delta, test) {
  return {
    topic () {
      const { callback } = this
      const json = _.cloneDeep(base)
      for (const path in delta) {
        const value = delta[path]
        _.set(json, path, value)
      }
      const options = {
        url: 'http://localhost:2342/agent',
        json,
        headers: {
          authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
        }
      }
      request.post(options, (err, response, body) => {
        if (err) {
          return callback(err)
        } else if (response.statusCode !== 200) {
          return callback(new Error(`Bad status code ${response.statusCode}: ${JSON.stringify(body)}`))
        } else {
          return callback(null, body)
        }
      })
      return undefined
    },
    'it works' (err, data) {
      assert.ifError(err)
    },
    'data looks correct' (err, data) {
      assert.ifError(err)
      assert.isObject(data)
      return test(data)
    },
    'and we get the agent endpoint': {
      topic (agent) {
        const { callback } = this
        const options = {
          url: `http://localhost:2342/agent/${agent.id}`,
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.get(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}: ${JSON.stringify(body)}`))
          } else {
            return callback(null, JSON.parse(body))
          }
        })
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'data looks correct' (err, data) {
        assert.ifError(err)
        assert.isObject(data)
        return test(data)
      }
    },
    'and we post in batch to the agent endpoint': {
      topic (agent) {
        const { callback } = this
        const options = {
          url: `http://localhost:2342/agent/${agent.id}`,
          json: _.map(__range__(40, 120, true), i => ({temperature: i})),
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}: ${JSON.stringify(body)}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'results looks correct' (err, data) {
        assert.ifError(err)
        assert.isArray(data)
        return (() => {
          const result1 = []
          for (const result of Array.from(data)) {
            assert.isObject(result)
            result1.push(assert.isNumber(result.fanSpeed))
          }
          return result1
        })()
      }
    }
  }
}

const updateBatch = function (delta, test) {
  return {
    topic () {
      const { callback } = this
      let json = _.cloneDeep(base)
      let options = {
        url: 'http://localhost:2342/agent',
        json,
        headers: {
          authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
        }
      }
      request.post(options, (err, response, body) => {
        if (err) {
          return callback(err)
        } else if (response.statusCode !== 200) {
          return callback(new Error(`Bad status code on creation ${response.statusCode}: ${JSON.stringify(body)}`))
        } else {
          json = _.cloneDeep(base)
          for (const path in delta) {
            const value = delta[path]
            _.set(json, path, value)
          }
          options = {
            url: `http://localhost:2342/agent/${body.id}`,
            json,
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          return request.put(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              return callback(new Error(`Bad status code on update ${response.statusCode}: ${JSON.stringify(body)}`))
            } else {
              return callback(null, body)
            }
          })
        }
      })
      return undefined
    },
    'it works' (err, data) {
      assert.ifError(err)
    },
    'data looks correct' (err, data) {
      assert.ifError(err)
      assert.isObject(data)
      return test(data)
    },
    'and we get the agent endpoint': {
      topic (agent) {
        const { callback } = this
        const options = {
          url: `http://localhost:2342/agent/${agent.id}`,
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.get(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, JSON.parse(body))
          }
        })
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'data looks correct' (err, data) {
        assert.ifError(err)
        assert.isObject(data)
        return test(data)
      }
    },
    'and we post in batch to the agent endpoint': {
      topic (agent) {
        const { callback } = this
        const options = {
          url: `http://localhost:2342/agent/${agent.id}`,
          json: _.map(__range__(40, 120, true), i => ({temperature: i})),
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}: ${JSON.stringify(body)}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'results looks correct' (err, data) {
        assert.ifError(err)
        assert.isArray(data)
        return (() => {
          const result1 = []
          for (const result of Array.from(data)) {
            assert.isObject(result)
            result1.push(assert.isNumber(result.fanSpeed))
          }
          return result1
        })()
      }
    }
  }
}

const assertValidDimension = function (data, prop, dim) {
  assert.isObject(data[prop])
  assert.isObject(data[prop][dim])
  return (() => {
    const result = []
    for (const setName in data[prop][dim]) {
      const points = data[prop][dim][setName]
      assert.isString(setName)
      assert.isArray(points)
      result.push(Array.from(points).map((point) =>
        assert.isNumber(point)))
    }
    return result
  })()
}

vows
  .describe('Using array for inputs and outputs')
  .addBatch(apiBatch({
    'and we create a new agent using an array for inputs':
      creationBatch(
        {'inputs.temperature': [50, 100, 3]}
        , data => assertValidDimension(data, 'inputs', 'temperature')),
    'and we create a new agent using an array for outputs':
      creationBatch(
        {'outputs.fanSpeed': [75, 175, 3]}
        , data => assertValidDimension(data, 'outputs', 'fanSpeed')),
    'and we update an agent using an array for inputs':
      updateBatch(
        {'inputs.temperature': [50, 100, 3]}
        , data => assertValidDimension(data, 'inputs', 'temperature')),
    'and we update an agent using an array for outputs':
      updateBatch(
        {'outputs.fanSpeed': [75, 175, 3]}
        , data => assertValidDimension(data, 'outputs', 'fanSpeed'))
  })).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}
