// api-get-user-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

vows
  .describe('GET /user does not exist')
  .addBatch(apiBatch({
    'and we request user data from the server': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/user',
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.get(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode === 404) {
            return callback(null)
          } else {
            return callback(new Error('Unexpected success'))
          }
        })
        return undefined
      },
      'it fails correctly' (err) {
        assert.ifError(err)
      }
    },
    'and we update the user data': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/user',
          json: {
            email: 'fakename2@mail.localhost'
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.put(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode === 404) {
            return callback(null)
          } else {
            return callback(new Error('Unexpected success'))
          }
        })
        return undefined
      },
      'it fails correctly' (err) {
        assert.ifError(err)
      }
    }
  })).export(module)
