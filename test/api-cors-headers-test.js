// api-cors-headers-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

vows
  .describe('CORS headers')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: [
              'IF temperature IS cold THEN fanSpeed IS slow',
              'IF temperature IS normal THEN fanSpeed IS normal',
              'IF temperature IS hot THEN fanSpeed IS fast'
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body, response.headers.location)
          }
        })
        return undefined
      },
      'it works' (err, data, location) {
        assert.ifError(err)
      },
      'and we do OPTIONS on the agent endpoint': {
        topic (agent, location) {
          const { callback } = this
          const options = {
            method: 'OPTIONS',
            url: location,
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if ((response.statusCode !== 204) && (response.statusCode !== 200)) {
              return callback(new Error(`Error ${response.statusCode}: ${JSON.stringify(body)}`))
            } else {
              return callback(null, response, body)
            }
          })
          return undefined
        },
        'it works' (err, response, body) {
          assert.ifError(err)
        },
        'it has the CORS headers' (err, response, data) {
          assert.ifError(err)
          assert.isObject(response)
          assert.isObject(response.headers)
          assert.isString(response.headers['access-control-allow-origin'])
          assert.isString(response.headers['access-control-allow-methods'])
          assert.isString(response.headers['access-control-allow-headers'])
        }
      }
    }}))

  .export(module)
