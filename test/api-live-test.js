// api-live-test.js
// Copyright 2017 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

vows
  .describe('GET /live')
  .addBatch(apiBatch({
    'and we get the /live endpoint': {
      topic (agent) {
        const { callback } = this
        request.get('http://localhost:2342/live', (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it returns null' (err, body) {
        assert.ifError(err)
        assert.isString(body)
        const live = JSON.parse(body)
        assert.isObject(live)
        assert.equal(live.status, 'OK')
      }
    }
  })).export(module)
