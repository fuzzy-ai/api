// lr.js
// Error reporter of last resort

const lr = function () {
  // If there are too many listeners

  if (process.listenerCount('uncaughtException') >= (process.getMaxListeners() - 1)) {
    process.setMaxListeners(process.getMaxListeners() + 1)
  }

  return process.on('uncaughtException', (err) => {
    console.log(err)
    return process.exit(-1)
  })
}

module.exports = lr
