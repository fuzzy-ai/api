// agentversion-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank

const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('AgentVersion')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const AgentVersion = require('../lib/agentversion')
          callback(null, AgentVersion)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, AgentVersion) {
        assert.ifError(err)
        assert.isFunction(AgentVersion)
      },
      'and we set up the database': {
        topic (AgentVersion) {
          const { callback } = this
          const params = JSON.parse(env.PARAMS)
          params.schema = {AgentVersion: AgentVersion.schema}
          const db = Databank.get(env.DRIVER, params)
          db.connect({}, (err) => {
            if (err) {
              return callback(err)
            } else {
              DatabankObject.bank = db
              return callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        },
        'and we create a new AgentVersion': {
          topic (AgentVersion) {
            const props = {
              inputs: {
                temperature: {
                  cold: [50, 75],
                  normal: [50, 75, 85, 100],
                  hot: [85, 100]
                }
              },
              outputs: {
                fanSpeed: {
                  slow: [50, 100],
                  normal: [50, 100, 150, 200],
                  fast: [150, 200]
                }
              },
              rules: [
                'IF temperature IS cold THEN fanSpeed IS slow',
                'IF temperature IS normal THEN fanSpeed IS normal',
                'IF temperature IS hot THEN fanSpeed IS fast'
              ]
            }
            AgentVersion.create({versionOf: '11112222', userID: 'aaaabbbbcc', properties: props}, this.callback)
            return undefined
          },
          'it works' (err, ver) {
            assert.ifError(err)
          },
          'and we examine it': {
            topic (ver) {
              return ver
            },
            'it has a unique ID' (err, ver) {
              assert.ifError(err)
              assert.isString(ver.id)
            },
            'it has the passed-in user ID' (err, ver) {
              assert.ifError(err)
              assert.equal(ver.userID, 'aaaabbbbcc')
            },
            'it has the passed-in versionOf' (err, ver) {
              assert.ifError(err)
              assert.equal(ver.versionOf, '11112222')
            },
            'it has a properties property' (err, ver) {
              assert.ifError(err)
              assert.isObject(ver.properties)
              assert.isObject(ver.properties.inputs)
              assert.isObject(ver.properties.outputs)
              assert.isArray(ver.properties.rules)
            },
            'it has a createdAt timestamp' (err, ver) {
              assert.ifError(err)
              assert.isString(ver.createdAt)
              assert.inDelta(Date.parse(ver.createdAt), Date.now(), 5000)
            }
          },
          'and we wait a couple of seconds': {
            topic (ver) {
              setTimeout(this.callback, 2000, null)
              return undefined
            },
            'it works' (err) {
              assert.ifError(err)
            },
            'and we try to update the AgentVersion object': {
              topic (ver) {
                const { callback } = this
                ver.update({versionOf: '33334444'}, (err, updated) => {
                  if (err) {
                    return callback(null)
                  } else {
                    return callback(new Error('Unexpected success'))
                  }
                })
                return undefined
              },
              'it fails correctly' (err, ver) {
                assert.ifError(err)
              }
            }
          }
        }
      }
    }}).export(module)
