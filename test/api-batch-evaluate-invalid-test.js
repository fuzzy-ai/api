// api-batch-evaluate-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const web = require('@fuzzy-ai/web')
const debug = require('debug')('api:api-batch-evaluate-invalid-test')

const apiBatch = require('./apibatch')

const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('POST invalid multiple inputs to /agent/:agentID')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        const { callback } = this

        const url = 'http://localhost:2342/agent'

        const body = {
          name: 'Thermometer',
          inputs: {
            temperature: {
              cold: [50, 75],
              normal: [50, 75, 85, 100],
              hot: [85, 100]
            }
          },
          outputs: {
            fanSpeed: {
              slow: [50, 100],
              normal: [50, 100, 150, 200],
              fast: [150, 200]
            }
          },
          rules: [
            'IF temperature IS cold THEN fanSpeed IS slow',
            'IF temperature IS normal THEN fanSpeed IS normal',
            'IF temperature IS hot THEN fanSpeed IS fast'
          ]
        }

        const headers = {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
        }

        web.post(url, headers, JSON.stringify(body), (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            debug(body)
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, JSON.parse(body))
          }
        })

        return undefined
      },

      'it works' (err, data) {
        assert.ifError(err)
        assert.isObject(data)
        assert.isString(data != null ? data.id : undefined)
      },
      'and we post invalid array input to the agent': {
        topic (agent) {
          const { callback } = this
          const url = `http://localhost:2342/agent/${agent.id}`
          const body = [
            {location: 'Cleveland, OH'},
            {temperature: 70}
          ]

          const headers = {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }

          web.post(url, headers, JSON.stringify(body), (err, response, body) => {
            if (err && (err.statusCode === 400)) {
              return callback(null, JSON.parse(err.body))
            } else if (!err) {
              return callback(new Error('Unexpected success'))
            } else {
              return callback(err)
            }
          })
          return undefined
        },
        'it fails correctly' (err, body) {
          assert.ifError(err)
          assert.isObject(body)
          assert.isString(body.status)
          assert.equal(body.status, 'error')
          assert.isString(body.message)
        }
      }
    }
  })).export(module)
