// accesstoken-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank

const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('AccessToken')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const AccessToken = require('../lib/accesstoken')
          callback(null, AccessToken)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, AccessToken) {
        assert.ifError(err)
        assert.isFunction(AccessToken)
      },
      'and we set up the database': {
        topic (AccessToken) {
          const { callback } = this
          const params = JSON.parse(env.PARAMS)
          params.schema =
            {accesstoken: AccessToken.schema}
          const db = Databank.get(env.DRIVER, params)
          db.connect({}, (err) => {
            if (err) {
              return callback(err)
            } else {
              DatabankObject.bank = db
              return callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        },
        'and we create a new AccessToken': {
          topic (AccessToken) {
            AccessToken.create({userID: 'aaaabbbbcc', app: 'default'}, this.callback)
            return undefined
          },
          'it works' (err, token) {
            assert.ifError(err)
          },
          'and we examine it': {
            topic (token) {
              return token
            },
            'it has the passed-in user ID' (err, token) {
              assert.ifError(err)
              assert.equal(token.userID, 'aaaabbbbcc')
            },
            'it has a token property' (err, token) {
              assert.ifError(err)
              assert.isString(token.token)
            },
            'it has the passed-in app ID' (err, token) {
              assert.ifError(err)
              assert.equal(token.app, 'default')
            },
            'it has a createdAt timestamp' (err, token) {
              assert.ifError(err)
              assert.isString(token.createdAt)
              assert.inDelta(Date.parse(token.createdAt), Date.now(), 5000)
            },
            'it has an updatedAt timestamp' (err, token) {
              assert.ifError(err)
              assert.isString(token.updatedAt)
              assert.inDelta(Date.parse(token.updatedAt), Date.now(), 5000)
            }
          }
        }
      }
    }}).export(module)
