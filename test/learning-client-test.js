// evaluator-client-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const LearningServerMock = require('./learning-server-mock')
const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('learning-client')
  .addBatch({
    'When we start a LearningServerMock': {
      topic () {
        const { callback } = this
        const srv = new LearningServerMock(env.LEARNING_KEY)
        srv.start((err) => {
          if (err) {
            return callback(err)
          } else {
            return callback(null, srv)
          }
        })
        return undefined
      },
      'it works' (err, srv) {
        assert.ifError(err)
        assert.isObject(srv)
      },
      teardown (srv) {
        srv.stop(this.callback)
      },
      'and we load the LearningClient module': {
        topic () {
          const { callback } = this
          try {
            const LearningClient = require('../lib/learning-client')
            callback(null, LearningClient)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, LearningClient) {
          assert.ifError(err)
          assert.isFunction(LearningClient)
        },
        'and we create an LearningClient': {
          topic (LearningClient) {
            const { callback } = this
            try {
              const client = new LearningClient(env.LEARNING_SERVER, env.LEARNING_KEY)
              callback(null, client)
            } catch (err) {
              callback(err)
            }
            return undefined
          },
          'it works' (err, client) {
            assert.ifError(err)
            assert.isObject(client)
          },
          'it has an optimize() method' (err, client) {
            assert.ifError(err)
            assert.isFunction(client.optimize)
          },
          'and we do an newAgent': {
            topic (client) {
              const { callback } = this
              const properties = {
                inputs: {
                  input1: {
                    low: [0, 1],
                    medium: [0, 1, 2],
                    high: [1, 2]
                  }
                },
                outputs: {
                  output1: {
                    low: [0, 1],
                    medium: [0, 1, 2],
                    high: [1, 2]
                  }
                },
                rules: ['input1 INCREASES output1'],
                parsed_rules: [
                  {type: 'increases', input: 'input1', output: 'output1'}
                ]
              }
              const inputs =
                {input1: 1.3}
              const expected =
                {output1: 1.5}
              client.optimize(properties, inputs, expected, (err, newAgent) => {
                if (err) {
                  return callback(err)
                } else {
                  return callback(null, newAgent)
                }
              })
              return undefined
            },
            'it works' (err, newAgent) {
              assert.ifError(err)
              assert.isObject(newAgent, `newAgent is not an object: ${newAgent}`)
              assert.isObject(newAgent.inputs,
                `newAgent.inputs is not an object: ${newAgent.inputs}`)
              assert.isObject(newAgent.outputs,
                `newAgent.outputs is not an object: ${newAgent.outputs}`)
              assert.isArray(newAgent.rules,
                `newAgent.rules is not an array: ${newAgent.rules}`)
              assert.isArray(newAgent.parsed_rules,
                `newAgent.parsed_rules isn't an array: ${newAgent.parsed_rules}`)
            }
          }
        }
      }
    }}).export(module)
