// apibatch-test.js
// Copyright 2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

vows
  .describe('API batch test')
  .addBatch(apiBatch({
    'and we request the version': {
      topic () {
        const { callback } = this
        const url = 'http://localhost:2342/version'
        request.get(url, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, JSON.parse(body))
          }
        })
        return undefined
      },
      'it works' (err, version) {
        assert.ifError(err)
        assert.isString(version.version)
        assert.isString(version.name)
      }
    }
  })).export(module)
