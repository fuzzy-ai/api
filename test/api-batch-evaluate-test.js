// api-batch-evaluate-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const web = require('@fuzzy-ai/web')

const apiBatch = require('./apibatch')

const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('POST multiple inputs to /agent/:agentID')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        const { callback } = this

        const url = 'http://localhost:2342/agent'

        const body = {
          name: 'Thermometer',
          inputs: {
            temperature: {
              cold: [50, 75],
              normal: [50, 75, 85, 100],
              hot: [85, 100]
            }
          },
          outputs: {
            fanSpeed: {
              slow: [50, 100],
              normal: [50, 100, 150, 200],
              fast: [150, 200]
            }
          },
          rules: [
            'IF temperature IS cold THEN fanSpeed IS slow',
            'IF temperature IS normal THEN fanSpeed IS normal',
            'IF temperature IS hot THEN fanSpeed IS fast'
          ]
        }

        const headers = {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
        }

        web.post(url, headers, JSON.stringify(body), (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, JSON.parse(body))
          }
        })

        return undefined
      },

      'it works' (err, data) {
        assert.ifError(err)
        assert.isObject(data)
        assert.isString(data != null ? data.id : undefined)
      },
      'and we post a bunch of input to the agent': {
        topic (agent) {
          const { callback } = this
          const temperatures = []
          for (let i = -10, asc = -10 <= 110; asc ? i <= 110 : i >= 110; asc ? i++ : i--) {
            temperatures.push({temperature: i})
          }
          const url = `http://localhost:2342/agent/${agent.id}`
          const headers = {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
          web.post(url, headers, JSON.stringify(temperatures), (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              return callback(new Error(`Bad status code ${response.statusCode}: ${JSON.stringify(body)}`))
            } else {
              return callback(null, response, JSON.parse(body))
            }
          })
          return undefined
        },
        'it works' (err, response, data) {
          assert.ifError(err)
        },
        'it has the right information' (err, response, data) {
          assert.ifError(err)
          assert.isArray(data)
          assert.lengthOf(data, 121)
          return (() => {
            const result = []
            for (const i of [0, 120]) {
              const obj = data[i]
              assert.isObject(obj)
              result.push(assert.isNumber(obj['fanSpeed']))
            }
            return result
          })()
        }
      }
    }}))
  .export(module)
