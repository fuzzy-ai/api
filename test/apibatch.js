// apibatch.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const childProcess = require('child_process')
const path = require('path')

const async = require('async')
const _ = require('lodash')

const env = require('./env')
const StatsServerMock = require('./stats-server-mock')
const PlanServerMock = require('./plan-server-mock')
const EvaluatorServerMock = require('./evaluator-server-mock')
const LearningServerMock = require('./learning-server-mock')

const base = {
  'When we start mock servers': {
    topic () {
      async.parallel([
        function (callback) {
          const srv = new PlanServerMock(env.PLAN_KEY)
          return srv.start((err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, srv)
            }
          })
        },
        function (callback) {
          const srv = new StatsServerMock(env.STATS_KEY)
          return srv.start((err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, srv)
            }
          })
        },
        function (callback) {
          const srv = new EvaluatorServerMock(env.EVALUATOR_KEY)
          return srv.start((err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, srv)
            }
          })
        },
        function (callback) {
          const srv = new LearningServerMock(env.LEARNING_KEY)
          return srv.start((err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, srv)
            }
          })
        }
      ], (err, results) => {
        if (err) {
          return this.callback(err)
        } else {
          const mocks = {
            plan: results[0],
            stats: results[1],
            evaluator: results[2],
            learning: results[3]
          }
          return this.callback(null, mocks)
        }
      })
      return undefined
    },
    'it works' (err, mocks) {
      assert.ifError(err)
      assert.isObject(mocks)
      assert.isObject(mocks.plan)
      assert.isObject(mocks.stats)
      assert.isObject(mocks.evaluator)
      assert.isObject(mocks.learning)
    },
    'teardown' (mocks) {
      const { callback } = this
      async.parallel([
        function (callback) {
          if (mocks.plan != null) {
            return mocks.plan.stop(callback)
          } else {
            return callback(null)
          }
        },
        function (callback) {
          if (mocks.stats != null) {
            return mocks.stats.stop(callback)
          } else {
            return callback(null)
          }
        },
        function (callback) {
          if (mocks.evaluator != null) {
            return mocks.evaluator.stop(callback)
          } else {
            return callback(null)
          }
        },
        function (callback) {
          if (mocks.learning != null) {
            return mocks.learning.stop(callback)
          } else {
            return callback(null)
          }
        }
      ], err => callback(err))
      return undefined
    },
    'and we run an API server': {
      topic () {
        const { callback } = this
        if (process.env.DEBUG != null) {
          env.DEBUG = process.env.DEBUG
        }
        const modulePath = path.join(__dirname, '..', 'lib', 'main.js')
        const child = childProcess.fork(modulePath, [], {env, silent: true})

        child.once('error', err => callback(err))

        child.stderr.on('data', (data) => {
          const str = data.toString('utf8')
          return process.stderr.write(`SERVER ERROR: ${str}\n`)
        })

        child.stdout.on('data', (data) => {
          const str = data.toString('utf8')
          if (str.match('Server listening')) {
            return callback(null, child)
          } else if (env.SILENT !== 'true') {
            return process.stdout.write(`SERVER: ${str}\n`)
          }
        })
        return undefined
      },
      'it works' (err, child) {
        assert.ifError(err)
        assert.isObject(child)
      },
      'teardown' (child) {
        return child.kill()
      }
    }
  }
}

const apiBatch = function (rest) {
  const batch = _.cloneDeep(base)
  const a = 'When we start mock servers'
  const b = 'and we run an API server'
  _.assign(batch[a][b], rest)
  return batch
}

module.exports = apiBatch
