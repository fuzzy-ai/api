// api-create-user-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')
const _ = require('lodash')

const apiBatch = require('./apibatch')
const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('POST /agent with empty properties')
  .addBatch(apiBatch({
    'and we create an empty agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            inputs: {},
            outputs: {},
            rules: []
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, agent) {
        assert.ifError(err)
      },
      'agent looks correct' (err, agent) {
        assert.ifError(err)
        assert.isObject(agent)
        assert.isObject(agent.inputs)
        assert.lengthOf(_.keys(agent.inputs), 0)
        assert.isObject(agent.outputs)
        assert.lengthOf(_.keys(agent.outputs), 0)
        assert.isArray(agent.rules)
        assert.lengthOf(agent.rules, 0)
      },
      'and we evaluate': {
        topic (agent) {
          const { callback } = this
          const options = {
            url: `http://localhost:2342/agent/${agent.id}`,
            json: {
              input1: 9.81
            },
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.post(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 400) {
              return callback(new Error(`Bad status code ${response.statusCode}`))
            } else {
              return callback(null, body)
            }
          })
          return undefined
        },
        'it fails correctly' (err, body) {
          assert.ifError(err)
          assert.isObject(body)
          assert.equal(body.status, 'error')
          assert.equal(body.message, 'Unrecognized argument \'input1\'.')
        }
      }
    }
  })).export(module)
