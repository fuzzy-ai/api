// statsclient-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const StatsServerMock = require('./stats-server-mock')
const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('statsclient')
  .addBatch({
    'When we start a StatsServerMock': {
      topic () {
        const { callback } = this
        const srv = new StatsServerMock(env.STATS_KEY)
        srv.start((err) => {
          if (err) {
            return callback(err)
          } else {
            return callback(null, srv)
          }
        })
        return undefined
      },
      'it works' (err, srv) {
        assert.ifError(err)
        assert.isObject(srv)
      },
      teardown (srv) {
        srv.stop(this.callback)
      },
      'and we load the statsclient module': {
        topic () {
          const { callback } = this
          try {
            const StatsClient = require('../lib/statsclient')
            callback(null, StatsClient)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, StatsClient) {
          assert.ifError(err)
          assert.isFunction(StatsClient)
        },
        'and we create a StatsClient': {
          topic (StatsClient) {
            const { callback } = this
            try {
              const statsClient = new StatsClient(env.STATS_SERVER, env.STATS_KEY)
              callback(null, statsClient)
            } catch (err) {
              callback(err)
            }
            return undefined
          },
          'it works' (err, client) {
            assert.ifError(err)
            assert.isObject(client)
          },
          'and we post a new evaluation log': {
            topic (client) {
              const { callback } = this
              const props = {
                reqID: 'TESTREQUESTID',
                userID: 'TESTUSERID',
                agentID: 'TESTAGENTID',
                versionID: 'TESTVERSIONID',
                input: {
                  temperature: 60,
                  pressure: 750
                },
                fuzzified: {
                  temperature: {
                    low: 0.6,
                    medium: 0.4,
                    high: 0.0
                  },
                  pressure: {
                    low: 0.3,
                    medium: 0.7,
                    high: 0.0
                  }
                },
                inferred: {
                  windChill: {
                    high: 0.3,
                    medium: 0.1,
                    low: 0.0
                  }
                },
                clipped: {
                  windChill: {
                    high: [[8, 0], [10, 0.3], [12, 0.3], [14, 0]],
                    medium: [[4, 0], [6, 0.1], [8, 0.1], [10, 0]],
                    low: [[0, 0], [1, 0], [2, 0], [3, 0]]
                  }
                },
                combined: {
                  windChill: [[4, 0], [6, 0.1], [8, 0.1], [10, 0.3], [12, 0.3], [14, 0]]
                },
                centroid: {
                  windChill: [9, 0.2]
                },
                crisp: {
                  windChill: 9
                }
              }
              client.newEvaluationLog(props, (err, log) => {
                if (err) {
                  return callback(err)
                } else {
                  return callback(null, log)
                }
              })
              return undefined
            },
            'it works' (err, log) {
              assert.ifError(err)
              assert.isObject(log)
              assert.isString(log.reqID)
              assert.isObject(log.input)
              assert.isObject(log.fuzzified)
              assert.isObject(log.inferred)
              assert.isObject(log.clipped)
              assert.isObject(log.combined)
              assert.isObject(log.centroid)
              assert.isObject(log.crisp)
            },
            'and we request the same evaluation log again': {
              topic (log, client) {
                client.getEvaluationLog(log.reqID, this.callback)
                return undefined
              },
              'it works' (err, log) {
                assert.ifError(err)
                assert.isObject(log)
                assert.isString(log.reqID)
                assert.isObject(log.input)
                assert.isObject(log.fuzzified)
                assert.isObject(log.inferred)
                assert.isObject(log.clipped)
                assert.isObject(log.combined)
                assert.isObject(log.centroid)
                assert.isObject(log.crisp)
              }
            },
            'and we post feedback to the log': {
              topic (log, client) {
                client.postFeedback(log.reqID, {performance: 3}, this.callback)
                return undefined
              },
              'it works' (err, feedback) {
                assert.ifError(err)
                assert.isObject(feedback)
                assert.isString(feedback.id)
                assert.isString(feedback.evaluationLog)
                assert.isObject(feedback.data)
                assert.isNumber(feedback.data.performance)
                assert.equal(feedback.data.performance, 3)
                assert.isString(feedback.createdAt)
                assert.inDelta(Date.parse(feedback.createdAt), Date.now(), 300000)
              },
              'and we get feedback from the log': {
                topic (feedback, log, client) {
                  client.getFeedback(log.reqID, this.callback)
                  return undefined
                },
                'it works' (err, feedbacks) {
                  assert.ifError(err)
                  assert.isArray(feedbacks)
                  assert.lengthOf(feedbacks, 1)
                  const [feedback] = feedbacks
                  assert.isObject(feedback)
                  assert.isString(feedback.id)
                  assert.isString(feedback.evaluationLog)
                  assert.isObject(feedback.data)
                  assert.isNumber(feedback.data.performance)
                  assert.equal(feedback.data.performance, 3)
                  assert.isString(feedback.createdAt)
                  assert.inDelta(Date.parse(feedback.createdAt), Date.now(), 300000)
                }
              }
            },
            'and we request the user usage': {
              topic (log, client) {
                const now = new Date()
                const year = now.getUTCFullYear()
                const mon = now.getUTCMonth() + 1
                client.getUserUsage(log.userID, year, mon, this.callback)
                return undefined
              },
              'it works' (err, usage) {
                assert.ifError(err)
                assert.isNumber(usage)
                assert.equal(1, usage)
              }
            }
          }
        }
      }
    }}).export(module)
