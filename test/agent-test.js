// agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank

const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('Agent')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const Agent = require('../lib/agent')
          callback(null, Agent)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, Agent) {
        assert.ifError(err)
        assert.isFunction(Agent)
      },
      'and we set up the database': {
        topic (Agent) {
          const { callback } = this
          const params = JSON.parse(env.PARAMS)
          params.schema = {Agent: Agent.schema}
          const db = Databank.get(env.DRIVER, params)
          db.connect({}, (err) => {
            if (err) {
              return callback(err)
            } else {
              DatabankObject.bank = db
              return callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        },
        'and we create a new agent': {
          topic (Agent) {
            const props = {
              inputs: {
                temperature: {
                  cold: [50, 75],
                  normal: [50, 75, 85, 100],
                  hot: [85, 100]
                }
              },
              outputs: {
                fanSpeed: {
                  slow: [50, 100],
                  normal: [50, 100, 150, 200],
                  fast: [150, 200]
                }
              },
              rules: [
                'IF temperature IS cold THEN fanSpeed IS slow',
                'IF temperature IS normal THEN fanSpeed IS normal',
                'IF temperature IS hot THEN fanSpeed IS fast'
              ]
            }
            Agent.create({userID: 'aaaabbbbcc', properties: props}, this.callback)
            return undefined
          },
          'it works' (err, fc) {
            assert.ifError(err)
          },
          'and we examine it': {
            topic (fc) {
              return fc
            },
            'it has the passed-in user ID' (err, fc) {
              assert.ifError(err)
              assert.equal(fc.userID, 'aaaabbbbcc')
            },
            'it has the passed-in properties' (err, fc) {
              assert.ifError(err)
              assert.isObject(fc.properties)
              assert.isObject(fc.properties.inputs)
              assert.isObject(fc.properties.outputs)
              assert.isArray(fc.properties.rules)
            },
            'it has a latestVersion property' (err, fc) {
              assert.ifError(err)
              assert.isString(fc.latestVersion)
            },
            'it has a createdAt timestamp' (err, fc) {
              assert.ifError(err)
              assert.isString(fc.createdAt)
              assert.inDelta(Date.parse(fc.createdAt), Date.now(), 5000)
            },
            'it has an updatedAt timestamp' (err, fc) {
              assert.ifError(err)
              assert.isString(fc.updatedAt)
              assert.inDelta(Date.parse(fc.updatedAt), Date.now(), 5000)
            }
          },
          'and we wait a couple of seconds': {
            topic (fc) {
              setTimeout(this.callback, 2000, null)
              return undefined
            },
            'it works' (err) {
              assert.ifError(err)
            },
            'and we update the Agent object': {
              topic (fc) {
                const oldVer = fc.latestVersion
                const { callback } = this
                const props = {
                  inputs: {
                    temperature: {
                      cold: [50, 80],
                      normal: [50, 80, 85, 100],
                      hot: [85, 100]
                    }
                  },
                  outputs: {
                    fanSpeed: {
                      slow: [50, 100],
                      normal: [50, 100, 150, 200],
                      fast: [150, 200]
                    }
                  },
                  rules: [
                    'IF temperature IS cold THEN fanSpeed IS slow',
                    'IF temperature IS normal THEN fanSpeed IS normal',
                    'IF temperature IS hot THEN fanSpeed IS fast'
                  ]
                }
                fc.update({properties: props}, (err, updated) => callback(err, updated, oldVer))
                return undefined
              },
              'it works' (err, fc, oldVer) {
                assert.ifError(err)
                assert.isObject(fc)
              },
              'its update timestamp is updated' (err, fc, oldVer) {
                assert.ifError(err)
                assert.notEqual(fc.updatedAt, fc.createdAt)
                assert.inDelta(Date.parse(fc.updatedAt), Date.now(), 5000)
              },
              'its properties are updated' (err, fc, oldVer) {
                assert.ifError(err)
                assert.equal(__guard__(__guard__(__guard__(__guard__(fc != null ? fc.properties : undefined, x3 => x3.inputs), x2 => x2.temperature), x1 => x1.cold), x => x[1]), 80)
                assert.equal(__guard__(__guard__(__guard__(__guard__(fc != null ? fc.properties : undefined, x7 => x7.inputs), x6 => x6.temperature), x5 => x5.normal), x4 => x4[1]), 80)
              },
              'its latestVersion is different' (err, fc, oldVer) {
                assert.ifError(err)
                assert.notEqual(fc.latestVersion, oldVer)
              }
            }
          }
        }
      }
    }}).export(module)

function __guard__ (value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined
}
