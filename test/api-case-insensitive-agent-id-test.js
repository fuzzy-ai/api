// api-create-user-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')
const _ = require('lodash')

const apiBatch = require('./apibatch')
const env = require('./env')
const lr = require('./lr')

lr()

const getAgent = function (transformer) {
  const batch = {
    topic (agent) {
      const transformed = transformer(agent.id)
      const options = {
        url: `http://localhost:2342/agent/${transformed}`,
        headers: {
          authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
        }
      }
      request.get(options, (err, response, body) => {
        if (err) {
          return this.callback(err)
        } else if (response.statusCode !== 200) {
          return this.callback(new Error(`Bad status code ${response.statusCode}: ${body}`))
        } else {
          return this.callback(null, JSON.parse(body))
        }
      })
      return undefined
    },
    'it works' (err, body) {
      assert.ifError(err)
      assert.isObject(body)
    }
  }
  return batch
}

vows
  .describe('Case-insensitive agent ID')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              'fan speed': {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: [
              'temperature INCREASES [fan speed]'
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'data looks correct' (err, data) {
        assert.ifError(err)
        assert.isObject(data)
        assert.isString(data.id)
      },
      'and we get the agent with verbatim agent ID':
        getAgent(_.identity),
      'and we get the agent with upcased agent ID':
        getAgent(_.toUpper),
      'and we get the agent with downcased agent ID':
        getAgent(_.toLower)
    }
  })).export(module)
