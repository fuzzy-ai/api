// api-delete-user-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')
const _ = require('lodash')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

vows
  .describe('DELETE /agent/:agentID')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: [
              'IF temperature IS cold THEN fanSpeed IS slow',
              'IF temperature IS normal THEN fanSpeed IS normal',
              'IF temperature IS hot THEN fanSpeed IS fast'
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, data) {
        assert.ifError(err)
      },
      'and we delete it': {
        topic (agent) {
          const { callback } = this
          const options = {
            url: `http://localhost:2342/agent/${agent.id}`,
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.del(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              return callback(new Error(`Bad status code ${response.statusCode}`))
            } else {
              return callback(null, body)
            }
          })
          return undefined
        },
        'it works' (err, data) {
          assert.ifError(err)
        },
        'and we get the agent endpoint': {
          topic (afterDelete, agent) {
            const { callback } = this
            const options = {
              url: `http://localhost:2342/agent/${agent.id}`,
              headers: {
                authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
              }
            }
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 410) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null, JSON.parse(body))
              }
            })
            return undefined
          },
          'it works' (err, data) {
            assert.ifError(err)
          }
        },
        'and we get the user agents endpoint': {
          topic (afterDelete, agent) {
            const { callback } = this
            const options = {
              url: 'http://localhost:2342/agent',
              headers: {
                authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
              }
            }
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 200) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null, JSON.parse(body), agent)
              }
            })
            return undefined
          },
          'it works' (err, data) {
            assert.ifError(err)
          },
          'it does not include our agent' (err, data, agent) {
            assert.ifError(err)
            assert(_.every(data, datum => datum !== agent.id))
          }
        }
      }
    }
  })).export(module)
