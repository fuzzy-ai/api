// api-create-user-agent-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const apiBatch = require('./apibatch')
const env = require('./env')
const lr = require('./lr')

lr()

vows
  .describe('POST /agent with empty rules')
  .addBatch(apiBatch({
    'and we create an empty agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: []
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, agent) {
        assert.ifError(err)
      },
      'agent looks correct' (err, agent) {
        assert.ifError(err)
        assert.isObject(agent)
        assert.isArray(agent.rules)
        assert.lengthOf(agent.rules, 0)
      },
      'and we evaluate': {
        topic (agent) {
          const { callback } = this
          const options = {
            url: `http://localhost:2342/agent/${agent.id}`,
            json: {
              temperature: 98.6
            },
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.post(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              return callback(new Error(`Bad status code ${response.statusCode}`))
            } else {
              return callback(null, body)
            }
          })
          return undefined
        },
        'it returns null' (err, body) {
          assert.ifError(err)
          assert.isObject(body)
          assert.isNull(body.fanSpeed)
        }
      }
    }
  })).export(module)
