// api-get-root-test.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

vows
  .describe('GET / returns HTML')
  .addBatch(apiBatch({
    'and we request the root document from the server': {
      topic () {
        const options =
          {url: 'http://localhost:2342/'}
        request.get(options, this.callback)
        return undefined
      },
      'it works' (err, response, body) {
        assert.ifError(err)
        assert.isObject(response)
        assert.equal(response.statusCode, 200)
        assert.isString(body)
      },
      'it returns an HTML document' (err, response, body) {
        assert.ifError(err)
        const expected = 'text/html'
        const ct = response.headers['content-type']
        const ctbase = ct != null ? ct.substr(0, expected.length) : undefined
        assert.equal(ctbase, expected)
      }
    }
  })).export(module)
