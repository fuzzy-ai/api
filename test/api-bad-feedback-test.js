// api-bad-feedback-test.js
// Copyright 2017 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const request = require('request')

const env = require('./env')
const apiBatch = require('./apibatch')
const lr = require('./lr')

lr()

const badFeedback = function (feedback) {
  const context = {
    topic (meta) {
      const options = {
        url: `http://localhost:2342/evaluation/${meta.reqID}/feedback`,
        json: feedback,
        headers: {
          authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
        }
      }
      request.post(options, (err, response, body) => {
        if (err) {
          return this.callback(err)
        } else if (response.statusCode === 400) {
          return this.callback(null)
        } else {
          return this.callback(new Error(`Unexpected status code: ${response.statusCode}`))
        }
      })
      return undefined
    },
    'it fails correctly' (err) {
      assert.ifError(err)
    }
  }
  return context
}

vows
  .describe('Input validation on POST /evaluation/:evaluationID/feedback')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/agent',
          json: {
            name: 'Thermometer',
            inputs: {
              temperature: {
                cold: [50, 75],
                normal: [50, 75, 85, 100],
                hot: [85, 100]
              }
            },
            outputs: {
              fanSpeed: {
                slow: [50, 100],
                normal: [50, 100, 150, 200],
                fast: [150, 200]
              }
            },
            rules: [
              'IF temperature IS cold THEN fanSpeed IS slow',
              'IF temperature IS normal THEN fanSpeed IS normal',
              'IF temperature IS hot THEN fanSpeed IS fast'
            ]
          },
          headers: {
            authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, agent) {
        assert.ifError(err)
        assert.isObject(agent)
      },
      'and we post to the agent': {
        topic (agent) {
          const { callback } = this
          const options = {
            url: `http://localhost:2342/agent/${agent.id}?meta=true`,
            json: {
              temperature: 60
            },
            headers: {
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
          }
          request.post(options, (err, response, body) => {
            if (err) {
              return callback(err)
            } else if (response.statusCode !== 200) {
              const code = response.statusCode
              const msg = body != null ? body.message : undefined
              return callback(new Error(`Error ${code}: ${msg}`))
            } else {
              return callback(null, body.meta)
            }
          })
          return undefined
        },
        'it works' (err, meta) {
          assert.ifError(err)
          assert.isObject(meta)
        },
        'and we post null for feedback': badFeedback(null),
        'and we post an empty object for feedback': badFeedback({}),
        'and we post an empty array for feedback': badFeedback([]),
        'and we post a string for feedback': badFeedback('some string'),
        'and we post a number for feedback': badFeedback(42),
        'and we post an array for feedback': badFeedback([3, 4, 5]),
        'and we post an object with an empty string key': badFeedback({'': 3}),
        'and we post an object with a string value': badFeedback({'performance': 'good'}),
        'and we post an object with an array value': badFeedback({'performance': [1, 2, 3]}),
        'and we post an object with an object value': badFeedback({'performance': {'mostly': 10}})
      }
    }
  })).export(module)
