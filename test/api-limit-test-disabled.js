// api-limit-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const async = require('async')
const _ = require('lodash')
const web = require('@fuzzy-ai/web')

const apiBatch = require('./apibatch')

const env = require('./env')
const lr = require('./lr')
const plans = require('./plans')

lr()

vows
  .describe('API limit')
  .addBatch(apiBatch({
    'and we create a new agent': {
      topic () {
        const { callback } = this
        const url = 'http://localhost:2342/agent'
        const properties = {
          inputs: {
            temperature: {
              cold: [50, 75],
              normal: [50, 75, 85, 100],
              hot: [85, 100]
            }
          },
          outputs: {
            fanSpeed: {
              slow: [50, 100],
              normal: [50, 100, 150, 200],
              fast: [150, 200]
            }
          },
          rules: [
            'IF temperature IS cold THEN fanSpeed IS slow',
            'IF temperature IS normal THEN fanSpeed IS normal',
            'IF temperature IS hot THEN fanSpeed IS fast'
          ]
        }
        const body = JSON.stringify(properties)
        const headers = {
          'Content-Type': 'application/json; charset=utf-8',
          'Content-Length': Buffer.byteLength(body),
          authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
        }

        web.post(url, headers, body, (err, response, body) => {
          if (err) {
            return callback(err)
          } else if (response.statusCode !== 200) {
            return callback(new Error(`Bad status code ${response.statusCode}`))
          } else {
            return callback(null, JSON.parse(body), response.headers.location)
          }
        })
        return undefined
      },
      'it works' (err, data, location) {
        assert.ifError(err)
      },
      'location header looks right' (err, data, location) {
        assert.ifError(err)
        assert.isString(location)
        assert.match(location, /^http:\/\/localhost:2342\/agent\/\w+$/)
      },
      'data looks correct' (err, data, location) {
        assert.ifError(err)
        assert.isObject(data)
        assert.isObject(data.inputs)
        assert.isObject(data.outputs)
        assert.isArray(data.rules)
        assert.isString(data.id)
        assert.isString(data.userID)
      },
      'and we do too many posts to the agent': {
        topic (agent, location) {
          const { callback } = this

          const succeeding = function (i, callback) {
            const url = location
            const props =
              {temperature: _.random(25, 125)}
            const body = JSON.stringify(props)
            const headers = {
              'Content-Type': 'application/json; charset=utf-8',
              'Content-Length': Buffer.byteLength(body),
              authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
            }
            return web.post(url, headers, body, (err, response, body) => {
              let msg
              if (err) {
                msg = `Error posting on iteration ${i}: ${err.message}`
                return callback(new Error(msg))
              } else if (response.statusCode !== 200) {
                const code = response.statusCode
                msg = `Agent failed on iteration ${i} before limit ${code}`
                return callback(new Error(msg))
              } else {
                //                process.stderr.write require('util').inspect(response.headers)
                return callback(null)
              }
            })
          }

          // See plans.js. This is the limit for the "free" plan

          const { apiLimit } = plans.free

          async.eachLimit(__range__(1, apiLimit, true), 4, succeeding, (err) => {
            if (err) {
              return callback(err)
            } else {
              const url = location
              const props =
                {temperature: 75}
              const body = JSON.stringify(props)
              const headers = {
                'Content-Type': 'application/json; charset=utf-8',
                'Content-Length': Buffer.byteLength(body),
                authorization: `Bearer ${env.ENSURE_ACCESS_TOKEN}`
              }

              // Now that we hit the limit, post one more time.

              return web.post(url, headers, body, (err, response, body) => {
                if (!err) {
                  return callback(new Error('Unexpected success'))
                } else if (err.statusCode !== 429) {
                  const code = err.statusCode
                  const msg = `Unexpected response code for last request ${code}`
                  return callback(new Error(msg))
                } else {
                  return callback(null, err.headers)
                }
              })
            }
          })

          return undefined
        },

        'it fails correctly' (err, headers) {
          assert.ifError(err)
        },
        'it has the correct Retry-After header' (err, headers) {
          assert.ifError(err)
          assert.isString(headers['retry-after'])
          const ra = Date.parse(headers['retry-after'])
          const now = Date.now()
          const DAY = 24 * 60 * 60 * 1000
          const MONTH = 31 * DAY
          assert.greater(ra, now)
          assert.lesser(ra, now + MONTH + DAY)
        },
        'it has the correct rate-limiting headers' (err, headers) {
          assert.ifError(err)
          assert.isString(headers['x-rate-limit-limit'])
          const limit = parseInt(headers['x-rate-limit-limit'], 10)
          assert.equal(limit, plans.free.apiLimit)
          assert.isString(headers['x-rate-limit-remaining'])
          const remaining = parseInt(headers['x-rate-limit-remaining'], 10)
          assert.equal(remaining, 0)
          assert.isString(headers['x-rate-limit-reset'])
          const reset = parseInt(headers['x-rate-limit-reset'], 10)
          const ra = Date.parse(headers['retry-after'])
          // It should be within a few seconds of the epoch
          assert.inDelta(ra, Date.now() + (reset * 1000), 5 * 1000)
        }
      }
    }
  })).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}
